//! This module contains the [`Task`] struct.

use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};

use crate::FileTime;

/// An operation to perform on the file system.
///
/// The most important tasks are [`CopyFile`](Task::CopyFile),
/// [`CreateDir`](Task::CreateDir) and [`CreateHardLink`](Task::CreateHardLink).
#[derive(Debug, Clone, PartialEq, Eq)]
#[derive(Serialize, Deserialize)]
pub enum Task {
    /// Creates the given directory.
    CreateDir(PathBuf),
    /// Sets the mtime of the given directory.
    SetMTime(PathBuf, FileTime),
    /// Copies file `src` to `dest` and sets the last-modified time
    /// of `dest` to `mtime`, if given.
    CopyFile {
        src: PathBuf,
        dest: PathBuf,
        mtime: Option<FileTime>,
    },
    /// Creates a hard link at `dest` pointing to `src`.
    CreateHardLink { src: PathBuf, dest: PathBuf },
    /// Creates a file-symlink at `dest` pointing to `content` and sets the
    /// last-modified time of `dest` to `mtime`, if given.
    ///
    /// `src` is the full path of the original symlink, which is to be copied.
    /// This is only used for informational purposes, e.g. to report errors.
    CreateSymlinkFile {
        src: PathBuf,
        dest: PathBuf,
        content: PathBuf,
        mtime: Option<FileTime>,
    },
    /// Creates a directory-symlink at `dest` pointing to `content` and sets the
    /// last-modified time of `dest` to `mtime`, if given.
    ///
    /// `src` is the full path of the original symlink, which is to be copied.
    /// This is only used for informational purposes, e.g. to report errors.
    CreateSymlinkDir {
        src: PathBuf,
        dest: PathBuf,
        content: PathBuf,
        mtime: Option<FileTime>,
    },
}

impl std::fmt::Display for Task {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::CreateDir(p) => write!(f, "Create Directory {}", p.display()),
            Self::SetMTime(_p, _) => Ok(()),
            Self::CopyFile { src, .. } => write!(f, "Write {}", src.display()),
            Self::CreateHardLink { src, .. } => write!(f, "Uptodate {}", src.display()),
            Self::CreateSymlinkFile { src, .. } | Self::CreateSymlinkDir { src, .. } => {
                write!(f, "Copy symlink {}", src.display())
            }
        }
    }
}

impl Task {
    #[allow(unused)]
    pub fn destination(&self) -> &Path {
        match self {
            Task::CreateDir(p) => p,
            Task::SetMTime(p, _) => p,
            Task::CopyFile {
                src: _,
                dest: p,
                mtime: _,
            } => p,
            Task::CreateHardLink { src: _, dest: p } => p,
            Task::CreateSymlinkFile { dest, .. } | Task::CreateSymlinkDir { dest, .. } => dest,
        }
    }
}

/// A collection of tasks which may be processed concurrently,
/// independent from each other.
///
/// Contains a set [`work`](TaskLane::work) of tasks which are
/// independent from each other and thus may be processed concurrently.
///
/// May in addition to that contain a set [`pre`](TaskLane::pre) of tasks which
/// are to be processed _sequentially_ before `work`, and a set [`post`](TaskLane::post)
/// of tasks which are to be processed after `work`, also _sequentially_.
///
/// In practice, `work` will be a set of files to copy, `pre` will be the directories
/// in which they are to copy, and `post` will be a set of
/// [`Task::SetMTime`]s, setting the last-modified times of the directories after
/// their content is written.
#[derive(Debug, Clone, Default)]
#[derive(Serialize, Deserialize)]
pub struct TaskLane {
    pub pre: Vec<Task>,
    pub work: Vec<Task>,
    pub post: Vec<Task>,
}

/// The task to copy a file, containing the complete content of the file
/// within the struct.
///
/// This can be used to send the task to copy a file to a remote site,
/// as a simple `fs::copy` cannot be used in that case, where
/// source and destination are on different machines.
#[derive(Debug, Clone)]
#[derive(Serialize, Deserialize)]
pub struct TaskCopyFile {
    pub dest: PathBuf,
    pub mtime: Option<FileTime>,
    pub content: Vec<u8>,
}

impl std::fmt::Display for TaskCopyFile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Write {}", self.dest.display())
    }
}

#[cfg(test)]
impl TaskLane {
    pub fn contains_create_dir<P: AsRef<Path>>(&self, dir: P) -> bool {
        self.pre.iter().any(|task| match task {
            Task::CreateDir(path) if path == dir.as_ref() => true,
            _ => false,
        })
    }
    pub fn contains_copy_file<P1: AsRef<Path>, P2: AsRef<Path>>(&self, src: P1, dest: P2) -> bool {
        self.work.iter().any(|task| match task {
            Task::CopyFile {
                src: src_this,
                dest: dest_this,
                mtime: _,
            } if src_this == src.as_ref() && dest_this == dest.as_ref() => true,
            _ => false,
        })
    }
    pub fn contains_create_hardlink<P1: AsRef<Path>, P2: AsRef<Path>>(
        &self,
        src: P1,
        dest: P2,
    ) -> bool {
        self.work.iter().any(|task| match task {
            Task::CreateHardLink {
                src: src_this,
                dest: dest_this,
            } if src_this == src.as_ref() && dest_this == dest.as_ref() => true,
            _ => false,
        })
    }
    #[allow(unused)]
    pub fn contains_copy_symlink_file<P1: AsRef<Path>, P2: AsRef<Path>, P3: AsRef<Path>>(
        &self,
        src: P1,
        dest: P2,
        content: P3,
    ) -> bool {
        self.work.iter().any(|task| match task {
            Task::CreateSymlinkFile {
                src: src_this,
                dest: dest_this,
                content: content_this,
                ..
            } if src_this == src.as_ref()
                && dest_this == dest.as_ref()
                && content_this == content.as_ref() =>
            {
                true
            }
            _ => false,
        })
    }
    pub fn contains_copy_symlink_dir<P1: AsRef<Path>, P2: AsRef<Path>, P3: AsRef<Path>>(
        &self,
        src: P1,
        dest: P2,
        content: P3,
    ) -> bool {
        self.work.iter().any(|task| match task {
            Task::CreateSymlinkDir {
                src: src_this,
                dest: dest_this,
                content: content_this,
                ..
            } if src_this == src.as_ref()
                && dest_this == dest.as_ref()
                && content_this == content.as_ref() =>
            {
                true
            }
            _ => false,
        })
    }
    pub fn contains_destination<P: AsRef<Path>>(&self, path: P) -> bool {
        self.pre.iter().any(|t| t.destination() == path.as_ref())
            || self.work.iter().any(|t| t.destination() == path.as_ref())
    }
    pub fn contains_set_mtime_for_dir<P: AsRef<Path>>(&self, dest: P) -> bool {
        self.post.iter().any(|task| match task {
            Task::SetMTime(dest_this, _) if dest_this == dest.as_ref() => true,
            _ => false,
        })
    }
}

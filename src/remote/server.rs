//! Contains functions necessary for remote synchronization on the
//! receiving side.
//!
//! Use [`serve`] to spin up a server which will process
//! synchronization attempts.

use std::string::ToString;
use std::sync::Arc;

use axum::{
    extract::{Json, Query, State},
    routing::{get, post},
    Router,
};
use axum_server::Handle;
use filetime::set_file_mtime;
use tokio::fs;

use super::{Basis, CheckSumQuery, ModifiedDateQuery};
use crate::error::ErrorPut;
use crate::receive::receive_local;
use crate::task::{Task, TaskCopyFile};
use crate::util::calc_checksum;
use crate::FileTime;
use crate::{Assignment, Config};

/// Wait for `Task`s coming in from a remote site
/// and perform them.
pub async fn serve(assignment: Assignment, config: Config) -> Result<(), std::io::Error> {
    let basis = Basis {
        assignment: Arc::new(assignment),
        config: Arc::new(config),
    };
    let app = Router::new()
        .route("/modified-date", get(modified_date))
        .route("/calc_checksum", get(checksum))
        .route("/copy_file", post(task_copy_file))
        .route("/task_other", post(task_other))
        .with_state(basis);

    axum_server::bind("127.0.0.1:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
}

/// Wait for `Task`s coming in from a remote site, but only print
/// them, instead of actually performing them.
pub async fn serve_and_print(assignment: Assignment, config: Config, handle: Option<Handle>) -> Result<(), std::io::Error> {
    let basis = Basis {
        assignment: Arc::new(assignment),
        config: Arc::new(config),
    };
    let app = Router::new()
        .route("/modified-date", get(modified_date))
        .route("/calc_checksum", get(checksum))
        .route("/copy_file", post(task_copy_file_print))
        .route("/task_other", post(task_other_print))
        .with_state(basis);

    let mut server = axum_server::bind("127.0.0.1:3000".parse().unwrap());
    if let Some(handle) = handle {
        server = server.handle(handle);
    }
    server.serve(app.into_make_service()).await
}

async fn modified_date(
    basis: State<Basis>,
    Query(params): Query<ModifiedDateQuery>,
) -> Result<Json<FileTime>, String> {
    let metadata = fs::symlink_metadata(
        &basis
            .0
            .assignment
            .origin_link
            .as_ref()
            .expect("Query ModifiedDate but no `origin_link` given")
            .join(params.path),
    )
    .await
    .map_err(|e| format!("{:?}", ErrorPut::SourceNoMetadata(e)))?;
    let mtime: FileTime = filetime::FileTime::from_last_modification_time(&metadata).into();
    Ok(Json(mtime))
}

async fn checksum(
    basis: State<Basis>,
    Query(params): Query<CheckSumQuery>,
) -> Result<Json<Option<[u8; 32]>>, String> {
    let path_link_dest = basis
        .0
        .assignment
        .origin_link
        .as_ref()
        .expect("Query checksum but no `origin_link` given")
        .join(params.path);
    let metadata = fs::symlink_metadata(&path_link_dest)
        .await
        .map_err(|e| format!("{:?}", ErrorPut::SourceNoMetadata(e)))
        .ok();
    match calc_checksum(path_link_dest, metadata, basis.0.config).await {
        Ok(res) => Ok(Json(res)),
        Err(_) => todo!(),
    }
}

async fn task_copy_file(
    basis: State<Basis>,
    Json(params): Json<TaskCopyFile>,
) -> Result<(), String> {
    let dest = basis.0.assignment.origin_destination.join(&params.dest);
    fs::write(&dest, &params.content)
        .await
        .map_err(|e| e.to_string())?;
    if let Some(mtime) = params.mtime {
        tokio::task::spawn_blocking(move || set_file_mtime(&dest, mtime.into()))
            .await
            .unwrap()
            .map_err(|e| e.to_string())?;
    }
    Ok(())
}

async fn task_other(basis: State<Basis>, Json(params): Json<Task>) -> Result<(), String> {
    if matches!(params, Task::CopyFile { .. }) {
        return Err(r#"Copying files must be done via the "copy_file" route"#.to_string());
    }

    receive_local::process_task(basis.0.assignment, params)
        .await
        .map_err(|e| format!("{:?}", e))
}

async fn task_copy_file_print(
    _basis: State<Basis>,
    Json(params): Json<TaskCopyFile>,
) -> Result<(), String> {
    println!("Incoming: {}", params);

    Ok(())
}

async fn task_other_print(_basis: State<Basis>, Json(params): Json<Task>) -> Result<(), String> {
    if matches!(params, Task::CopyFile { .. }) {
        return Err(r#"Copying files must be done via the "copy_file" route"#.to_string());
    }

    println!("Incoming: {}", params);

    Ok(())
}

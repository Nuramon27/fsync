//! Contains structures for remote synchronization.
//!
//! You will mainly need [`RemoteInfo`] as the collection
//! of information about the server to which the files
//! shall go.

pub mod server;

use std::path::PathBuf;
use std::sync::Arc;

use reqwest::Client;
use serde::{Deserialize, Serialize};

use crate::{Assignment, Config};

/// Collects information about the server to which the files shall go.
///
/// This is mainly the `url`.
#[derive(Debug, Clone)]
pub struct RemoteInfo {
    pub client: Client,
    pub url: String,
}

impl RemoteInfo {
    pub fn new(url: String) -> Self {
        RemoteInfo {
            client: Client::new(),
            url,
        }
    }
}

#[derive(Debug, Clone)]
struct Basis {
    assignment: Arc<Assignment>,
    config: Arc<Config>,
}

/// A serializable struct used to query the last-modified date
/// of a file on the remote site.
#[derive(Debug, Clone)]
#[derive(Serialize, Deserialize)]
pub struct ModifiedDateQuery {
    path: PathBuf,
}

/// A serializable struct used to query the last-modified date
/// of a file on the remote site.
#[derive(Debug, Clone)]
#[derive(Serialize, Deserialize)]
pub struct CheckSumQuery {
    path: PathBuf,
}

use std::cmp;
use std::fs::Metadata;
use std::io;
use std::path::PathBuf;
use std::sync::Arc;

use blake3::Hasher;
use tokio::fs::OpenOptions;
use tokio::io::AsyncReadExt;

use crate::error::ErrorLinkDest;
use crate::Config;

pub async fn calc_checksum(
    path_link_dest: PathBuf,
    metadata: Option<Metadata>,
    config: Arc<Config>,
) -> Result<Option<[u8; 32]>, ErrorLinkDest> {
    let size = if let Some(metadata) = metadata {
        cmp::min(metadata.len(), 0x8 * 0x400 * 0x400)
    } else {
        // 0,5 MB
        0x200 * 0x400
    };
    let mut buf = bytes::BytesMut::with_capacity(size as usize);
    let mut hasher = Hasher::new();
    {
        let mut file = match OpenOptions::new().read(true).open(&path_link_dest).await {
            Ok(file) => file,
            Err(err) if matches!(err.kind(), io::ErrorKind::NotFound) => return Ok(None),
            Err(err) => {
                if config.save_when_error_on_link {
                    return Ok(None);
                } else {
                    return Err(ErrorLinkDest::CalcChecksumLocal {
                        path: path_link_dest.clone(),
                        err,
                    });
                }
            }
        };
        loop {
            AsyncReadExt::read_buf(&mut file, &mut buf)
                .await
                .map_err(|err| ErrorLinkDest::CalcChecksumLocal {
                    path: path_link_dest.clone(),
                    err,
                })?;
            let buf_slice = &buf[..];
            if buf_slice.len() == 0 {
                break;
            }
            hasher.update(buf_slice);
            buf.clear();
        }
    }
    Ok(Some(hasher.finalize().as_bytes().to_owned()))
}

use std::io;
use std::path::PathBuf;

use crate::Task;

#[derive(Debug)]
pub enum ErrorFsync {
    Traverse(ErrorTraverse),
    Execute(ErrorExecute),
    Join(tokio::task::JoinError),
}

impl From<ErrorTraverse> for ErrorFsync {
    fn from(value: ErrorTraverse) -> Self {
        ErrorFsync::Traverse(value)
    }
}

impl From<ErrorExecute> for ErrorFsync {
    fn from(value: ErrorExecute) -> Self {
        ErrorFsync::Execute(value)
    }
}

#[derive(Debug)]
pub enum ErrorPut {
    OnLinkDestination(io::Error),
    SourceNoMetadata(io::Error),
    OnCopy(io::Error),
    OnSymlinkFile(io::Error),
    OnSymlinkDir(io::Error),
    OnLink(io::Error),
    OnCreateDir(io::Error),
    OnSetMTime(io::Error),
    Connection(reqwest::Error),
}

#[derive(Debug)]
pub enum ErrorExecute {
    OnLinkDestination { task: Task, err: io::Error },
    SourceNoMetadata { task: Task, err: io::Error },
    OnCopy { task: Task, err: io::Error },
    OnSymlinkFile { task: Task, err: io::Error },
    OnSymlinkDir { task: Task, err: io::Error },
    OnLink { task: Task, err: io::Error },
    OnCreateDir { task: Task, err: io::Error },
    OnSetMTime { task: Task, err: io::Error },
    Connection { task: Task, err: reqwest::Error },
}

#[derive(Debug)]
pub enum ErrorWalk {
    NextEntryInDir { path: PathBuf, err: io::Error },
    Descend { path: PathBuf, err: io::Error },
    NoFileType { path: PathBuf, err: io::Error },
}

#[derive(Debug)]
pub enum ErrorMetadata {
    SourceNoMetadata {
        path: PathBuf,
        err: io::Error,
    },
    DestNoMetadata(ErrorLinkDest),
    OnLinkDestination {
        path: PathBuf,
        err: io::Error,
    },
    UnkownIfSymlink {
        path: PathBuf,
        err: io::Error,
    },
    Connection {
        path: PathBuf,
        err: reqwest::Error,
    },
    JoinWhileRetrievingMetadata {
        path: PathBuf,
        err: tokio::task::JoinError,
    },
}

#[derive(Debug)]
pub enum ErrorLinkDest {
    NoMetadataLocal { path: PathBuf, err: io::Error },
    JoinWhileRetrievingMetadata { path: PathBuf, err: io::Error },
    Connection { path: PathBuf, err: reqwest::Error },
    CalcChecksumLocal { path: PathBuf, err: io::Error },
    CalcChecksumRemote { path: PathBuf, err: io::Error },
}

#[derive(Debug)]
pub enum ErrorTraverse {
    StartDir(io::Error),
    Traverse(ErrorWalk),
    Metadata(ErrorMetadata),
    Join(tokio::task::JoinError),
}

impl From<ErrorWalk> for ErrorTraverse {
    fn from(value: ErrorWalk) -> Self {
        ErrorTraverse::Traverse(value)
    }
}

impl From<ErrorMetadata> for ErrorTraverse {
    fn from(value: ErrorMetadata) -> Self {
        ErrorTraverse::Metadata(value)
    }
}

impl From<tokio::task::JoinError> for ErrorTraverse {
    fn from(value: tokio::task::JoinError) -> Self {
        ErrorTraverse::Join(value)
    }
}

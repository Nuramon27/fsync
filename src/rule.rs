use std::cmp::Ordering;
use std::ffi::OsString;

use std::path::{self, Component, Path};

use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug, Clone)]
pub struct Rule {
    absolute: bool,
    is_dir: bool,
    r: Vec<Regex>,
}

#[derive(Debug, Clone)]
pub enum RuleCreateError {
    Regex(regex::Error),
    InvalidComponent,
    Utf8(OsString),
}

lazy_static! {
    static ref RULE_COMPONENT: Regex = Regex::new(r"\*|\?|[^\*\?]*").unwrap();
}

impl Rule {
    fn path_designates_dir(path: &Path) -> bool {
        if let Some(path_str) = path.to_str() {
            path_str
                .chars()
                .last()
                .map(|c| path::is_separator(c))
                .unwrap_or(false)
        } else {
            false
        }
    }
    fn parse_component(s: &str) -> Result<Regex, RuleCreateError> {
        let mut res = String::new();
        res.push_str("^");
        for part in RULE_COMPONENT.find_iter(s) {
            match part.as_str() {
                "*" => res.push_str(r".*"),
                "?" => res.push_str(r".?"),
                part => res.push_str(&regex::escape(part)),
            }
        }
        res.push_str("$");
        Ok(Regex::new(&res).map_err(|e| RuleCreateError::Regex(e))?)
    }
    pub fn from_glob(path: &Path) -> Result<Self, RuleCreateError> {
        let mut absolute = false;
        let mut r = Vec::new();
        for comp in path.components() {
            match comp {
                Component::Normal(s) => r.push(Self::parse_component(
                    &s.to_os_string()
                        .into_string()
                        .map_err(|e| RuleCreateError::Utf8(e))?,
                )?),
                Component::CurDir => (),
                Component::RootDir => absolute = true,
                _comp @ (Component::ParentDir | Component::Prefix(_)) => {
                    return Err(RuleCreateError::InvalidComponent)
                }
            }
        }

        let is_dir = Self::path_designates_dir(path);

        Ok(Rule {
            absolute,
            r,
            is_dir,
        })
    }
    pub fn max_pos(&self, other: &Path) -> usize {
        if self.absolute {
            1
        } else {
            other
                .components()
                .filter(|p| {
                    !matches!(
                        p,
                        Component::RootDir
                            | Component::CurDir
                            | Component::ParentDir
                            | Component::Prefix(_)
                    )
                })
                .count()
        }
    }
    fn _is_match_at(&self, pos: usize, other: &Path, is_dir: bool, maybe: bool) -> bool {
        if self.absolute && pos > 0 {
            return false;
        }
        let mut other_iter = other
            .components()
            .filter(|p| {
                !matches!(
                    p,
                    Component::RootDir
                        | Component::CurDir
                        | Component::ParentDir
                        | Component::Prefix(_)
                )
            })
            .skip(pos)
            .peekable();

        let mut is_match;
        let exhausted;
        {
            is_match = other_iter.peek().is_some();
            for rule in self.r.iter() {
                match other_iter.next() {
                    Some(Component::Normal(other_comp)) => {
                        let other_comp = other_comp.to_string_lossy();
                        if !rule.is_match(&other_comp) {
                            is_match = false;
                            break;
                        }
                    }
                    // unreachable because filtered out
                    Some(
                        Component::RootDir
                        | Component::CurDir
                        | Component::ParentDir
                        | Component::Prefix(_),
                    ) => unreachable!(),
                    None => {
                        if !maybe {
                            is_match = false;
                        }
                        break;
                    }
                }
            }
            exhausted = !other_iter.peek().is_some();
        }
        if exhausted {
            is_match &= !self.is_dir || is_dir;
        }
        is_match
    }
    pub fn is_match_at(&self, pos: usize, other: &Path, is_dir: bool) -> bool {
        self._is_match_at(pos, other, is_dir, false)
    }
    pub fn may_match_at(&self, pos: usize, other: &Path, is_dir: bool) -> bool {
        self._is_match_at(pos, other, is_dir, true)
    }
    pub fn cmp_ending_pos(this: (&Self, usize), other: (&Self, usize)) -> Ordering {
        (this.1 + this.0.r.len()).cmp(&(other.1 + other.0.r.len()))
    }
    pub fn cmp_length(&self, other: &Self) -> Ordering {
        self.r.len().cmp(&other.r.len())
    }
    pub fn cmp_relativity(&self, other: &Self) -> Ordering {
        match (self.absolute, other.absolute) {
            (true, true) | (false, false) => Ordering::Equal,
            (true, false) => Ordering::Greater,
            (false, true) => Ordering::Less,
        }
    }
}

pub fn must_include(dir: &Path, excludes: &[Rule], includes: &[Rule]) -> bool {
    for exclude in excludes {
        'excludes: for pos_ex in 0..exclude.max_pos(dir) {
            if exclude.is_match_at(pos_ex, dir, false) {
                for include in includes {
                    for pos_in in 0..std::cmp::min(pos_ex + 1, include.max_pos(dir)) {
                        if (include.r.len() + pos_in >= exclude.r.len() + pos_ex)
                            && (include.absolute || !exclude.absolute)
                        {
                            if include.is_match_at(pos_in, dir, false) {
                                continue 'excludes;
                            }
                        }
                    }
                }
                return false;
            }
        }
    }
    true
}

pub fn must_descend(dir: &Path, excludes: &[Rule], includes: &[Rule]) -> bool {
    for exclude in excludes {
        'excludes: for pos_ex in 0..exclude.max_pos(dir) {
            if exclude.is_match_at(pos_ex, dir, true) {
                for include in includes {
                    for pos_in in 0..std::cmp::min(pos_ex + 1, include.max_pos(dir)) {
                        if (include.r.len() + pos_in >= exclude.r.len() + pos_ex)
                            && (include.absolute || !exclude.absolute)
                        {
                            if include.may_match_at(pos_in, dir, true) {
                                continue 'excludes;
                            }
                        }
                    }
                }
                return false;
            }
        }
    }
    true
}

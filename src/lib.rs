//!
//!
//! # Architecture of `fsync`
//!
//! Looking for files to sync and actually copying these
//! are two tasks which can mostly be done concurrently, in a
//! pipelined fashion:
//!
//! A _traverse-task_ starts at the origin directory
//! [`Assignment::origin_source`] and traverses it recursively, entering directories
//! and selecting files to be synced according to a set of [`Rules`]. For every
//! directory to create in the destination folder and every file to copy,
//! a `Task` is created and sent down a channel to the _receive-task_.
//! This tokio-task receives incoming these and performs them, doing so concurrently
//! when possible.

#![allow(rustdoc::redundant_explicit_links)]

pub mod error;
mod own;
mod receive;
pub mod remote;
pub mod rule;
mod task;
#[cfg(test)]
mod tests;
mod traverse;
mod util;

use std::future::Future;
use std::path::PathBuf;

use error::ErrorFsync;
use remote::RemoteInfo;
use serde::{Deserialize, Serialize};
use tokio::sync::broadcast;
use tokio::sync::mpsc::{channel, unbounded_channel, Receiver, Sender, UnboundedSender};
use tokio::{join, select};

use receive::{receive_and_copy_fs, receive_and_print, receive_and_send_to_remote};
use task::{Task, TaskLane};
use traverse::{traverse_local, traverse_remote};

pub use own::FileTime;
pub use rule::Rule;

/// Contains the base path for soucre and destination, as well
/// as for the link destination.
#[derive(Debug, Clone)]
pub struct Assignment {
    /// The base path from which the files are copied.
    ///
    /// `fsync` will scan this directory recursively and according
    /// to [`Rules`] for files which are to be copied.
    pub origin_source: PathBuf,
    /// The destination path to which the files are copied.
    ///
    /// A file copied from path `[origin_source]/[relative_path]`
    /// will be copied to `[origin_destination]/[relative_path]`.
    pub origin_destination: PathBuf,
    /// The destination for links.
    ///
    /// If a file in path `[origin_source]/[relative_path]`
    /// also exists in `[origin_link]/[relative_path]` and has the
    /// same last-modified date there as the original, a hard link
    /// to `[origin_link]/[relative_path]` is created instead of copying
    /// the file.
    pub origin_link: Option<PathBuf>,
}

/// General configuration settings to control the behaviour of `fsync`.
#[derive(Debug, Clone, Copy)]
#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct Config {
    /// Whether a multithreaded runtime is to be used. Default true.
    pub multithreaded: bool,
    /// If an error occurrs while `fsync` is checking the last-modified
    /// date of a file in [`origin_link`](Assignment::origin_link), the behaviour
    /// depends on this setting: If it is true, the file is simply copied
    /// and traversing goes on. If it is false, traversing is aborted
    /// and an error is returned.
    ///
    /// Default false.
    pub save_when_error_on_link: bool,
    /// The maximum number of tasks that are processed in one batch.
    ///
    /// `Task`s in one batch can be processed concurrently to some degree.
    /// This setting controls how many tasks are collected before they
    /// are processed.
    ///
    /// Default 256.
    pub max_tasks_per_lane: usize,
    pub max_metadata_tasks: usize,
    /// The maximum number of tasks that may be processed concurrently.
    ///
    /// `fsync` will not try to process all
    /// [`max_tasks_per_lane`](Config::max_tasks_per_lane)
    /// concurrently, but will always ever queue `max_concurrent_tasks`
    /// for concurrent processing.
    ///
    /// Default 8.
    pub max_concurrent_tasks: usize,
    pub buffer_length_tasks: usize,
    pub compare_checksums: bool,
    pub short_circuit_on_different_mtime: bool,
    pub remote: bool,
}

#[derive(Debug, Clone, Default)]
pub struct Rules {
    pub excludes: Vec<Rule>,
    pub includes: Vec<Rule>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            multithreaded: true,
            save_when_error_on_link: false,
            max_tasks_per_lane: 256,
            max_metadata_tasks: 256,
            max_concurrent_tasks: 8,
            buffer_length_tasks: 16,
            compare_checksums: false,
            short_circuit_on_different_mtime: false,
            remote: false,
        }
    }
}

/// A collection of some statistics about the sync process.
#[derive(Debug, Clone, Copy)]
pub struct SyncResult {
    /// The total size of files newly written.
    ///
    /// This includes only files which were actually copied,
    /// not the hardlinked ones.
    size_written: f64,
    /// The number of files newly written.
    ///
    /// This includes only files which were actually copied,
    /// not the hardlinked ones.
    files_written: usize,
    /// The total size of files that are synced.
    ///
    /// This includes copied and hardlinked files.
    size_in_sync: f64,
    /// The number of files that are synced.
    ///
    /// This includes copied and hardlinked files.
    files_in_sync: usize,
    /// The `Instant` at which syncing was started.
    start: std::time::Instant,
}

impl Default for SyncResult {
    fn default() -> Self {
        Self::new()
    }
}

impl std::fmt::Display for SyncResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(
            f,
            "Total nr of files: {}; Files written: {}",
            self.files_in_sync, self.files_written
        )?;
        writeln!(
            f,
            "Total size: {};    Bytes written: {}",
            Self::format_bytes(self.size_in_sync),
            Self::format_bytes(self.size_written)
        )?;
        write!(f, "Elapsed time: {:?}", self.start.elapsed())
    }
}

impl SyncResult {
    /// Interprets `x` as a size in bytes and prints
    /// it with a sensible SI-prefix.
    fn format_bytes(x: f64) -> String {
        if x < 1024.0 {
            format!("{:.0} B", x)
        } else if x < 1024.0f64.powi(2) {
            format!("{:.3} KB", x / 1024.0f64.powi(1))
        } else if x < 1024.0f64.powi(3) {
            format!("{:.3} MB", x / 1024.0f64.powi(2))
        } else if x < 1024.0f64.powi(4) {
            format!("{:.3} GB", x / 1024.0f64.powi(3))
        } else {
            format!("{:.4} TB", x / 1024.0f64.powi(4))
        }
    }
    pub(crate) fn new() -> Self {
        SyncResult {
            size_written: 0.0,
            files_written: 0,
            size_in_sync: 0.0,
            files_in_sync: 0,
            start: std::time::Instant::now(),
        }
    }
    /// Updates this `SyncResult` with `task`, obtaining
    /// the file size from `metadata`.
    pub(crate) fn update(&mut self, task: &Task, metadata: &std::fs::Metadata) {
        match task {
            Task::CreateDir(..) | Task::SetMTime(..) => (),
            Task::CopyFile { .. }
            | Task::CreateSymlinkFile { .. }
            | Task::CreateSymlinkDir { .. } => {
                let len = metadata.len();
                self.size_in_sync += len as f64;
                self.size_written += len as f64;
                self.files_in_sync += 1;
                self.files_written += 1;
            }
            Task::CreateHardLink { src: _, dest: _ } => {
                self.size_in_sync += metadata.len() as f64;
                self.files_in_sync += 1;
            }
        }
    }
}

/// Executes future `func`, but concurrently waits for an incoming message
/// on `abort_channel`. If a message arrives before `func` is finished,
/// this function returns `None` and aborts `func`.
///
/// Otherwise, the `Output` of `func` is returned.
pub async fn func_or_abort<F: Future<Output = Out>, Out>(
    func: F,
    mut abort_channel: broadcast::Receiver<()>,
) -> Option<Out> {
    select! {
        res = func => Some(res),
        _ = abort_channel.recv() => None,
    }
}

/// The general function to synchronize folders.
///
/// This spins up the traverse-task, in which `traverse_func` is executed,
/// and the receive-task, where `receiver_func` is executed. See
/// [Architecture of `fsync`](crate#architecture-of-fsync) for details.
///
/// An additional error-tasks waits for errors on a channel, aborting
/// the other tasks if an error comes in. In this case, that error is returned.
async fn sync_general<
    FRec: FnOnce(Assignment, Config, Receiver<TaskLane>, UnboundedSender<ErrorFsync>) -> RRec,
    FTrav: FnOnce(Assignment, Config, Rules, Sender<TaskLane>, UnboundedSender<ErrorFsync>) -> RTrav,
    RRec: Future<Output = Out> + Send + 'static,
    RTrav: Future<Output = SyncResult> + Send + 'static,
    Out: Send + 'static,
>(
    assignment: Assignment,
    config: Config,
    rules: Rules,
    receiver_func: FRec,
    traverse_func: FTrav,
) -> Result<(Out, SyncResult), Vec<ErrorFsync>> {
    let (message_ser, message_rec) = channel(config.buffer_length_tasks);
    let (err_ser, mut err_rec) = unbounded_channel();
    let (abort_ser, abort_rec) = tokio::sync::broadcast::channel(1);

    let abort_rec = (abort_rec, abort_ser.subscribe(), abort_ser.subscribe());
    let err_task = tokio::spawn(async move {
        if let Some(e) = err_rec.recv().await {
            abort_ser.clone().send(()).unwrap();
            Err(vec![e])
        } else {
            Ok(())
        }
    });
    let (info, res, err) = join! {
        tokio::spawn(func_or_abort(
            traverse_func(assignment.clone(), config.clone(), rules.clone(), message_ser, err_ser.clone()),
            abort_rec.0
        )),
        tokio::spawn(func_or_abort(
            receiver_func(assignment, config, message_rec, err_ser),
            abort_rec.1
        )),
        err_task
    };
    match (info, res, err) {
        (_, _, Ok(Err(e))) => Err(e),
        (Ok(Some(info)), Ok(Some(res)), Ok(Ok(()))) => Ok((res, info)),
        // unreachable panic because when `err_task` does not return an error,
        // then `func_or_abort` definitively returns `Some`.
        (Ok(None), Ok(None), Ok(Ok(())))
        | (Ok(None), Ok(Some(_)), Ok(Ok(())))
        | (Ok(Some(_)), Ok(None), Ok(Ok(()))) => unreachable!(),
        (_, _, Err(e)) | (_, Err(e), _) | (Err(e), _, _) => Err(vec![ErrorFsync::Join(e)]),
    }
}

/// Performs a local synchronization of folders listed in `assignment` and
/// according to `rules`, where source and destination are on the same machine.
pub async fn sync_fs_local(
    assignment: Assignment,
    config: Config,
    rules: Rules,
) -> Result<SyncResult, Vec<ErrorFsync>> {
    sync_general(
        assignment,
        config,
        rules,
        receive_and_copy_fs,
        traverse_local,
    )
    .await
    .map(|(_, a)| a)
}

/// Performs a synchronization of folders listed in `assignment` and
/// according to `rules` where the destination is on a remote site.
pub async fn sync_remote(
    assignment: Assignment,
    config: Config,
    rules: Rules,
    rinfo: RemoteInfo,
) -> Result<SyncResult, Vec<ErrorFsync>> {
    let rinfo_2 = rinfo.clone();
    sync_general(
        assignment,
        config,
        rules,
        |assignment: Assignment,
         config: Config,
         messager: Receiver<TaskLane>,
         error_channel: UnboundedSender<ErrorFsync>| {
            receive_and_send_to_remote(assignment, config, messager, error_channel, rinfo_2)
        },
        |assignment: Assignment,
         config: Config,
         rules: Rules,
         messager: Sender<TaskLane>,
         error_channel: UnboundedSender<ErrorFsync>| {
            traverse_remote(assignment, config, rules, messager, error_channel, rinfo)
        },
    )
    .await
    .map(|(_, a)| a)
}

/// Performs a dry run of local synchronization of folders listed in `assignment` and
/// according to `rules`, where source and destination are on the same machine.
///
/// This will not copy any files, but only print out the created tasks.
pub async fn sync_dry_run_local(
    assignment: Assignment,
    config: Config,
    rules: Rules,
) -> Result<SyncResult, Vec<ErrorFsync>> {
    sync_general(assignment, config, rules, receive_and_print, traverse_local)
        .await
        .map(|(_, a)| a)
}

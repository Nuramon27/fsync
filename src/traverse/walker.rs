use std::fs::FileType;
use std::path::PathBuf;

use futures::Stream;
use tokio::fs;
use tokio::fs::{DirEntry, ReadDir};

use crate::error::ErrorWalk;
use crate::rule::{must_descend, must_include};
use crate::Rules;

/// The type of item yielded by the [`DirWalker`].
#[derive(Debug)]
pub enum DirAction {
    /// A normal entry of a directory, carrying the corresponding `DirEntry`, its `FileType`
    /// and the relative path.
    Entry(DirEntry, FileType, PathBuf),
    /// The information that the walker left a directory and "ascended" into a higher one.
    ///
    /// This is necessary because the [`crate::traverse::traverse`]-function needs this information
    /// in order to set the modification time after the last entry of a directory has been touched.
    Ascend(PathBuf),
}

/// A DirWalker traverses a directory recursively and depth-first
/// and yields every entry restricted by the set of [`rules`](DirWalker::rules).
struct DirWalker {
    /// As a `ReadDir` is an iterator through the contents of a directory,
    /// the `dir_stack` contains the full information on which file/folder
    /// we are currently processing at each level of the filesystem.
    dir_stack: Vec<ReadDir>,
    /// This simply holds the path we are currently in, as seen relatively
    /// from the starting point [`origin_source`](DirWalker::origin_source).
    relative_path: PathBuf,
    /// The rules restricting which directory entries to yield
    /// and into which directories to descend.
    rules: Rules,
    /// The starting directory of the traversal
    origin_source: PathBuf,
}

impl DirWalker {
    /// Wrapper around [`_dir_next`](DirWalker::_dir_next) which takes an owned `Self` and returns it again.
    ///
    /// This satisfies the signature demanded by [`futures::stream::unfold`].
    async fn dir_next(mut self) -> Option<(Result<DirAction, ErrorWalk>, Self)> {
        match self._dir_next().await {
            Ok(Some(entry)) => Some((Ok(entry), self)),
            Ok(None) => None,
            Err(e) => Some((Err(e), self)),
        }
    }
    /// Asynchronously get the next entry in the directory tree.
    ///
    /// If this entry is a directory itself, we have to descend into it. If it is any other file,
    /// we simply yield it, having advanced the `ReadDir` of the current directory. If we have exhausted
    /// all recursive entries in the current directory, we ascend one level, yielding one
    /// [`DirAction::Ascend`].
    async fn _dir_next(&mut self) -> Result<Option<DirAction>, ErrorWalk> {
        while !self.dir_stack.is_empty() {
            let read_dir = self
                .dir_stack
                .last_mut()
                // Won't panic because `dir_stack` is non-empty
                .unwrap();
            loop {
                match read_dir
                    .next_entry()
                    .await
                    .map_err(|err| ErrorWalk::NextEntryInDir {
                        path: self.origin_source.join(self.relative_path.clone()),
                        err,
                    })? {
                    Some(entry) => {
                        let name = entry.file_name();
                        let file_type =
                            entry
                                .file_type()
                                .await
                                .map_err(|err| ErrorWalk::NoFileType {
                                    path: self.origin_source.join(self.relative_path.join(&name)),
                                    err,
                                })?;
                        if file_type.is_dir() {
                            if must_descend(
                                entry
                                    .path()
                                    .strip_prefix(&self.origin_source)
                                    .expect("Directory in source is not a subdirectory of origin."),
                                &self.rules.excludes,
                                &self.rules.includes,
                            ) {
                                // Descend into the directory and yield it.
                                let new_read_dir = fs::read_dir(
                                    &self.origin_source.join(&self.relative_path).join(&name),
                                )
                                .await
                                .map_err(|err| ErrorWalk::Descend {
                                    path: self.origin_source.join(self.relative_path.join(&name)),
                                    err,
                                })?;
                                self.relative_path.push(name);
                                self.dir_stack.push(new_read_dir);
                                return Ok(Some(DirAction::Entry(
                                    entry,
                                    file_type,
                                    self.relative_path.clone(),
                                )));
                            }
                        } else {
                            if must_include(
                                entry
                                    .path()
                                    .strip_prefix(&self.origin_source)
                                    .expect("Directory in source is not a subdirectory of origin."),
                                &self.rules.excludes,
                                &self.rules.includes,
                            ) {
                                return Ok(Some(DirAction::Entry(
                                    entry,
                                    file_type,
                                    self.relative_path.clone(),
                                )));
                            }
                        }
                    }
                    None => {
                        // Go up one level, but save the relative path of the current directory in order
                        // to yield it as a `DirAction::Ascend`.
                        let old_relative_path = self.relative_path.clone();
                        self.relative_path.pop();
                        self.dir_stack.pop();
                        return Ok(Some(DirAction::Ascend(old_relative_path)));
                    }
                }
            }
        }
        Ok(None)
    }
}

/// Returns a stream which traverses `start_dir` recursively and depth-first
/// and yields every entry restricted by the set of `rules`.
///
/// `origin_source` must be identical to the path of `start_dir`.
pub fn dir_walker(
    start_dir: ReadDir,
    rules: Rules,
    origin_source: PathBuf,
) -> impl Stream<Item = Result<DirAction, ErrorWalk>> {
    let walker = DirWalker {
        dir_stack: vec![start_dir],
        relative_path: PathBuf::new(),
        rules,
        origin_source,
    };
    // `DirWalker::dir_next` is built such that it mutates the walker and yields an
    // item every time it is called. That way it can be used to build a Stream
    // via futures::stream::unfold.
    futures::stream::unfold(walker, DirWalker::dir_next)
}

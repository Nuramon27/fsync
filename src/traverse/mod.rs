//! Functions for the _traverse-task_,
//! which manage the recursive traversal of a directory
//! according to a set of [`Rules`](crate::Rules).
//!
//! Use [`traverse_local`] for an all local traversal, and
//! [`traverse_remote`] if the link destination is on a remote site.
//!
//! See [Architecture of `fsync`](crate#architecture-of-fsync)
//! for details on how these functions are used in fsync.

mod create_tasks;
mod put;
mod walker;

use futures::StreamExt as StreamExtFut;

use tokio::sync::mpsc::{Sender, UnboundedSender};

use tokio::fs;

use crate::error::{ErrorFsync, ErrorTraverse};
use crate::remote::RemoteInfo;
use crate::task::TaskLane;
use crate::{Assignment, Config, Rules, SyncResult};
use create_tasks::Putter;
use walker::dir_walker;

/// Performs a depth first search on the [`origin_source`](Assignment::origin_source) directory
/// in `assignment`, excluding files and directories according to `rules`.
async fn traverse(
    assignment: Assignment,
    config: Config,
    rules: Rules,
    messager: Sender<TaskLane>,
    error_channel: UnboundedSender<ErrorFsync>,
    rinfo: Option<RemoteInfo>,
) -> SyncResult {
    let start_dir = match fs::read_dir(&assignment.origin_source).await {
        Ok(start_dir) => start_dir,
        Err(err) => {
            error_channel
                .send(ErrorTraverse::StartDir(err).into())
                .expect("Error channel closed");
            return SyncResult::default();
        }
    };

    let walker = dir_walker(start_dir, rules.clone(), assignment.origin_source.clone());
    let state = Putter::new(assignment, messager, rinfo, config, error_channel.clone());
    // `Putter::handle_dir_action` is built such that it accumulates results in `state`
    // for every item (every `DirAction`), with which it is called.
    // Thus it is fit for use with futures::StreamExt::fold in order to accumlate
    // tasks in it while going through the walker-Stream.
    let state = StreamExtFut::fold(walker, state, |state, dir_action| async {
        match dir_action {
            Ok(dir_action) => Putter::handle_dir_action(state, dir_action, &error_channel).await,
            Err(err) => {
                error_channel
                    .send(ErrorTraverse::Traverse(err).into())
                    .expect("Error channel closed");
                state
            }
        }
    })
    .await;
    // Send the last lane and consume the `state`.
    match state.send_lane_and_drop().await {
        Ok(result) => result,
        Err(err) => {
            error_channel
                .send(err.into())
                .expect("Error channel closed");
            SyncResult::default()
        }
    }
}

pub async fn traverse_local(
    assignment: Assignment,
    config: Config,
    rules: Rules,
    messager: Sender<TaskLane>,
    error_channel: UnboundedSender<ErrorFsync>,
) -> SyncResult {
    traverse(assignment, config, rules, messager, error_channel, None).await
}

pub async fn traverse_remote(
    assignment: Assignment,
    config: Config,
    rules: Rules,
    messager: Sender<TaskLane>,
    error_channel: UnboundedSender<ErrorFsync>,
    rinfo: RemoteInfo,
) -> SyncResult {
    traverse(
        assignment,
        config,
        rules,
        messager,
        error_channel,
        Some(rinfo),
    )
    .await
}

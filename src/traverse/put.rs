//! The put-functions obtain metadata of files and directories
//! and create tasks from these.

use std::io;
use std::path::{Path, PathBuf};
use std::sync::Arc;

use tokio::fs;
use tokio::spawn;
use tokio::sync::RwLock;

use crate::error::{ErrorLinkDest, ErrorMetadata};
use crate::remote::RemoteInfo;
use crate::task::Task;
use crate::util::calc_checksum;
use crate::FileTime;
use crate::{Assignment, Config, SyncResult};

pub async fn put_directory(
    assignment: &Assignment,
    relative_path: &Path,
    result: &RwLock<SyncResult>,
) -> Task {
    let path_source = assignment.origin_source.join(relative_path);
    let task = Task::CreateDir(relative_path.to_owned());
    println!("{}", task);
    if let Ok(metadata) = fs::symlink_metadata(&path_source).await {
        let mut result = RwLock::write(&result).await;
        result.update(&task, &metadata);
    }

    task
}

pub async fn put_mtime(
    assignment: &Assignment,
    relative_path: &Path,
    _result: &RwLock<SyncResult>,
) -> Option<Task> {
    let path_source = assignment.origin_source.join(relative_path);
    let metadata = fs::symlink_metadata(&path_source).await.ok()?;
    let mtime = filetime::FileTime::from_last_modification_time(&metadata).into();
    Some(Task::SetMTime(relative_path.to_owned(), mtime))
}

pub async fn put_symlink(
    assignment: &Assignment,
    relative_path: &Path,
    result: &RwLock<SyncResult>,
) -> Result<Task, io::Error> {
    let path_source = assignment.origin_source.join(relative_path);
    let metadata_maybe = fs::symlink_metadata(&path_source).await;
    let mtime = metadata_maybe
        .as_ref()
        .map(|metadata| filetime::FileTime::from_last_modification_time(metadata).into())
        .ok();
    let content = fs::read_link(&path_source).await?;
    let task = if let Ok(metadata_linkination) = fs::metadata(&path_source).await {
        if metadata_linkination.is_dir() {
            Task::CreateSymlinkDir {
                src: relative_path.to_owned(),
                dest: relative_path.to_owned(),
                content,
                mtime,
            }
            // It is not clear, whether `create_symlink_dir` or `create_symlink_file`
            // should be called if the target itself is a symlink. But we default to a file.
        } else if metadata_linkination.is_file() || metadata_linkination.is_symlink() {
            Task::CreateSymlinkFile {
                src: relative_path.to_owned(),
                dest: relative_path.to_owned(),
                content,
                mtime,
            }
        } else {
            unreachable!()
        }
    } else {
        Task::CreateSymlinkFile {
            src: relative_path.to_owned(),
            dest: relative_path.to_owned(),
            content,
            mtime,
        }
    };
    println!("{}", task);
    if let Ok(metadata) = metadata_maybe {
        let mut result = RwLock::write(&result).await;
        result.update(&task, &metadata);
    }

    Ok(task)
}

pub async fn put(
    assignment: Arc<Assignment>,
    relative_path: PathBuf,
    config: Arc<Config>,
    result: Arc<RwLock<SyncResult>>,
    rinfo: Arc<Option<RemoteInfo>>,
) -> Result<Task, ErrorMetadata> {
    let path_source = assignment.origin_source.join(&relative_path);
    let metadata_source_fut = spawn(fs::symlink_metadata(path_source.clone()));
    let metadata_source;
    let task = if let Some(origin_link) = &assignment.origin_link {
        let path_link_dest = origin_link.join(&relative_path);
        let metadata_link_fut;
        if !(config.compare_checksums && config.short_circuit_on_different_mtime) {
            metadata_link_fut = Some(spawn(get_mtime_there(
                relative_path.clone(),
                Arc::clone(&assignment),
                Arc::clone(&config),
                Arc::clone(&rinfo),
            )));
        } else {
            metadata_link_fut = None;
        }

        metadata_source = metadata_source_fut
            .await
            .map_err(|err| ErrorMetadata::JoinWhileRetrievingMetadata {
                path: path_source.clone(),
                err,
            })?
            .map_err(|err| ErrorMetadata::SourceNoMetadata {
                path: path_source.clone(),
                err,
            })?;
        let checksum_source_fut;
        let checksum_link_fut;
        if config.compare_checksums {
            checksum_source_fut = Some(spawn(calc_checksum(
                path_source.clone(),
                Some(metadata_source.clone()),
                Arc::clone(&config),
            )));
            checksum_link_fut = Some(spawn(calc_checksum_there(
                relative_path.clone(),
                Arc::clone(&rinfo),
                Arc::clone(&assignment),
                Arc::clone(&config),
            )));
        } else {
            checksum_source_fut = None;
            checksum_link_fut = None;
        }

        let mtime_link;
        if !(config.compare_checksums && config.short_circuit_on_different_mtime) {
            mtime_link = metadata_link_fut
                // Won't panic because metadat_link was set in identical branch
                .unwrap()
                .await
                .map_err(|err| ErrorMetadata::JoinWhileRetrievingMetadata {
                    path: path_link_dest.clone(),
                    err,
                })?
                .map_err(|err| ErrorMetadata::DestNoMetadata(err))?;
        } else {
            mtime_link = None;
        }
        let checksum_source;
        let checksum_link;
        if config.compare_checksums {
            checksum_source = checksum_source_fut
                // Won't panic because metadat_link was set in identical branch
                .unwrap()
                .await
                .map_err(|err| ErrorMetadata::JoinWhileRetrievingMetadata {
                    path: path_link_dest.clone(),
                    err,
                })?
                .map_err(|err| ErrorMetadata::DestNoMetadata(err))?;
            checksum_link = checksum_link_fut
                // Won't panic because metadat_link was set in identical branch
                .unwrap()
                .await
                .map_err(|err| ErrorMetadata::JoinWhileRetrievingMetadata {
                    path: path_link_dest.clone(),
                    err,
                })?
                .map_err(|err| ErrorMetadata::DestNoMetadata(err))?;
        } else {
            checksum_source = None;
            checksum_link = None;
        }

        let mtime_source = filetime::FileTime::from_last_modification_time(&metadata_source).into();

        match should_copy(
            Some(mtime_source),
            mtime_link,
            checksum_source.as_ref(),
            checksum_link.as_ref(),
            &config,
        ) {
            true => Ok(Task::CopyFile {
                src: relative_path.clone(),
                dest: relative_path,
                mtime: Some(mtime_source),
            }),
            false => Ok(Task::CreateHardLink {
                src: relative_path.clone(),
                dest: relative_path,
            }),
        }
    } else {
        metadata_source = metadata_source_fut
            .await
            .map_err(|err| ErrorMetadata::JoinWhileRetrievingMetadata {
                path: path_source.clone(),
                err,
            })?
            .map_err(|err| ErrorMetadata::SourceNoMetadata {
                path: path_source.clone(),
                err,
            })?;
        let mtime_source =
            Some(filetime::FileTime::from_last_modification_time(&metadata_source).into());
        Ok(Task::CopyFile {
            src: relative_path.clone(),
            dest: relative_path,
            mtime: mtime_source,
        })
    };

    if let Ok(task) = &task {
        println!("{}", task);
        {
            let mut result = RwLock::write(&result).await;
            result.update(task, &metadata_source);
        }
    }

    task
}

pub async fn get_mtime_there(
    relative_path: PathBuf,
    assignment: Arc<Assignment>,
    config: Arc<Config>,
    rinfo: Arc<Option<RemoteInfo>>,
) -> Result<Option<FileTime>, ErrorLinkDest> {
    let rinfo = Option::as_ref(&rinfo);
    if config.remote {
        match rinfo
            .expect("remote info neccessary in remote mode")
            .client
            .get(&rinfo.expect("Remote info neccessary in remote mode").url)
            .query(&[("path", &relative_path)])
            .send()
            .await
        {
            Ok(r) => match r.json::<Option<FileTime>>().await {
                Ok(modified_link) => Ok(modified_link),
                Err(err) => {
                    if config.save_when_error_on_link {
                        Ok(None)
                    } else {
                        Err(ErrorLinkDest::Connection {
                            path: relative_path,
                            err,
                        })
                    }
                }
            },
            Err(err) => Err(ErrorLinkDest::Connection {
                path: relative_path,
                err,
            }),
        }
    } else {
        let path_link_dest = assignment.origin_link.as_ref().unwrap().join(relative_path);
        match fs::symlink_metadata(&path_link_dest).await {
            Ok(metadata_link) => Ok(Some(
                filetime::FileTime::from_last_modification_time(&metadata_link).into(),
            )),
            Err(err) if matches!(err.kind(), io::ErrorKind::NotFound) => Ok(None),
            Err(err) => {
                if config.save_when_error_on_link {
                    Ok(None)
                } else {
                    Err(ErrorLinkDest::NoMetadataLocal {
                        path: path_link_dest,
                        err,
                    })
                }
            }
        }
    }
}

async fn calc_checksum_there(
    relative_path: PathBuf,
    rinfo: Arc<Option<RemoteInfo>>,
    assignment: Arc<Assignment>,
    config: Arc<Config>,
) -> Result<Option<[u8; 32]>, ErrorLinkDest> {
    let rinfo = Option::as_ref(&rinfo);
    if config.remote {
        match rinfo
            .expect("remote info neccessary in remote mode")
            .client
            .get(&rinfo.expect("Remote info neccessary in remote mode").url)
            .query(&[("checksum", &relative_path)])
            .send()
            .await
        {
            Ok(r) => match r.json::<Option<[u8; 32]>>().await {
                Ok(checksum) => Ok(checksum),
                Err(err) => {
                    if config.save_when_error_on_link {
                        Ok(None)
                    } else {
                        Err(ErrorLinkDest::Connection {
                            path: relative_path,
                            err,
                        })
                    }
                }
            },
            Err(err) => Err(ErrorLinkDest::Connection {
                path: relative_path,
                err,
            }),
        }
    } else {
        let path_link_dest = assignment.origin_link.as_ref().unwrap().join(relative_path);
        let metadata = fs::symlink_metadata(&path_link_dest).await.ok();
        calc_checksum(path_link_dest, metadata, config.clone()).await
    }
}

pub fn should_copy(
    modified_source: Option<FileTime>,
    modified_link: Option<FileTime>,
    checksum_source: Option<&[u8; 32]>,
    checksum_link: Option<&[u8; 32]>,
    config: &Config,
) -> bool {
    let mut res = false;
    if !(config.compare_checksums && config.short_circuit_on_different_mtime) {
        res |= modified_link
            .map(|modified_link| modified_link < modified_source.unwrap())
            .unwrap_or(true);
    }
    if config.compare_checksums {
        res |= checksum_link
            .map(|checksum_link| checksum_link != checksum_source.unwrap())
            .unwrap_or(true);
    }
    res
}

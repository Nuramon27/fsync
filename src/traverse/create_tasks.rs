use std::fmt;
use std::future::Future;
use std::mem;
use std::pin::Pin;
use std::sync::Arc;

use either::{Either, Left, Right};
use futures::StreamExt as StreamExtFut;

use tokio::spawn;

use tokio::sync::mpsc::{self, Sender, UnboundedReceiver, UnboundedSender};
use tokio::sync::RwLock;
use tokio::task::JoinError;

use tokio_stream::wrappers::ReceiverStream;

use super::put::{put, put_directory, put_mtime, put_symlink};
use super::walker::DirAction;
use crate::error::{ErrorFsync, ErrorMetadata, ErrorTraverse};
use crate::remote::RemoteInfo;
use crate::task::{Task, TaskLane};
use crate::{Assignment, Config, SyncResult};

/// The TaskAwaiter manages creating tasks and awaiting their definition.
///
/// We do not want to call all the put-methods (see [`put`](crate::traverse::put)) serially,
/// as some of them (in particular, querying the remote side and calculating checksums)
/// may take a while to obtain their metadata. So instead we send futures executing these
/// put-functions to the spawned task
/// [`spawn_put`](TaskAwaiter::spawn_put), where they complete cuncurrently. The results
/// of the put-functions are sent back and awaited in the function
/// [`await_tasks_and_drop`](TaskAwaiter::await_tasks_and_drop).
///
/// A task of which the creation may take longer can be enqueued via
/// [`enqueue_work_pending`](TaskAwaiter::enqueue_work_pending). Tasks of which
/// the creation is fast can use the remaining enqueue-functions. For pre- and post-tasks,
/// this is the only option, as they must be run in order and thus cannot make the way through
/// the `spawn_put`, which would mix up their order.
///
/// The final result of a TaskAwaiter is a [`TaskLane`](crate::task::TaskLane). As `TaskLane`s
/// have a maximum number of tasks in them, we regularly check whether the number of enqueued
/// tasks has reached this maximum number. If so, no further tasks can be enqueued,
/// but the still pending tasks are awaited and the final `TaskLane` is returned.
///
/// This is the reason why the enqueue-functions take ownership of `self`: If the threshold
/// of tasks is exceeded, they consume `self` and return the final `TaskLane` instead.
struct TaskAwaiter<Fut: Future<Output = Result<Task, ErrorMetadata>> + Send + 'static> {
    lane_current: TaskLane,
    /// The [`JoinHandle`](tokio::task::JoinHandle) of the spawned task where the
    /// long-running put-functions are enqueued.
    spawn_put: tokio::task::JoinHandle<()>,
    /// The sender by which futures of the put-functions are sent to the
    /// [`spawn_put`](TaskAwaiter::spawn_put)-task.
    ser_metadata: Sender<Fut>,
    /// The receiver by which created tasks are sent back.
    rec_task: UnboundedReceiver<Task>,
    /// The number of tasks that will eventually land in the pre-part of the final `TaskLane`
    nr_pending_pre: usize,
    /// The number of tasks that will eventually land in the work-part of the final `TaskLane`
    nr_pending_work: usize,
}

impl<Fut: Future<Output = Result<Task, ErrorMetadata>> + Send + 'static> fmt::Debug
    for TaskAwaiter<Fut>
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("PendingManager")
            .field("lane_current", &self.lane_current)
            .field("obtain_metadata", &self.spawn_put)
            .field("rec_task", &self.rec_task)
            .field("nr_pending_pre", &self.nr_pending_pre)
            .field("nr_pending_work", &self.nr_pending_work)
            .finish_non_exhaustive()
    }
}

impl<Fut: Future<Output = Result<Task, ErrorMetadata>> + Send> TaskAwaiter<Fut> {
    /// Create a new `TaskAwaiter` and spawn the [`spawn_put`](TaskAwaiter::spawn_put).
    fn new(config: &Config, error_channel: UnboundedSender<ErrorFsync>) -> Self {
        let (ser_metadata, rec_metadata) = mpsc::channel::<Fut>(config.max_metadata_tasks);
        let (ser_task, rec_task) = mpsc::unbounded_channel::<Task>();
        let spawn_put = spawn(ReceiverStream::new(rec_metadata).for_each_concurrent(
            config.max_concurrent_tasks,
            move |fut| {
                let ser_task = ser_task.clone();
                let error_channel = error_channel.clone();
                async move {
                    match fut.await {
                        Ok(task) => ser_task
                            .send(task)
                            .expect("Receiver of metadata tasks closed"),
                        Err(err) => error_channel
                            .send(ErrorFsync::Traverse(ErrorTraverse::Metadata(err)))
                            .expect("Error channel closed"),
                    }
                }
            },
        ));
        Self {
            lane_current: TaskLane::default(),
            spawn_put,
            ser_metadata,
            rec_task,
            nr_pending_pre: 0,
            nr_pending_work: 0,
        }
    }

    /// Await the creation of pending tasks in `spawn_put` and construct
    /// the final `TaskLane` from all created tasks.
    pub async fn await_tasks_and_drop(mut self) -> Result<TaskLane, JoinError> {
        // Drop the sender for metadata queries, so the `spawn_put` stops waiting
        // for such queries and completes.
        mem::drop(self.ser_metadata);
        self.spawn_put.await?;
        while let Some(task) = self.rec_task.recv().await {
            self.lane_current.work.push(task);
        }
        Ok(self.lane_current)
    }

    /// Check whether the number of pending and created tasks is still below the specified
    /// threshold. If it is still below, `self` is returned as is. Otherwise,
    /// `self` is consumed and the remaining tasks are awaited.
    async fn maybe_await_tasks(self, config: &Config) -> Result<Either<Self, TaskLane>, JoinError> {
        if self.nr_pending_pre >= config.max_tasks_per_lane
            || self.nr_pending_work >= config.max_tasks_per_lane
        {
            let res = self.await_tasks_and_drop().await?;
            Ok(Right(res))
        } else {
            Ok(Left(self))
        }
    }

    /// Add a pre-task without checking overflow of the threshold.
    fn enqueue_pre_unchecked(&mut self, task: Task) {
        self.nr_pending_pre += 1;
        self.lane_current.pre.push(task);
    }

    /// Add a pre-task
    async fn enqueue_pre(
        mut self,
        task: Task,
        config: &Config,
    ) -> Result<Either<Self, TaskLane>, JoinError> {
        self.nr_pending_pre += 1;
        self.lane_current.pre.push(task);
        self.maybe_await_tasks(config).await
    }

    /// Add a post-task
    async fn enqueue_post(
        mut self,
        task: Task,
        config: &Config,
    ) -> Result<Either<Self, TaskLane>, JoinError> {
        self.lane_current.post.push(task);
        self.maybe_await_tasks(config).await
    }

    /// Add a work-task that is already completely created.
    async fn enqueue_work_ready(
        mut self,
        task: Task,
        config: &Config,
    ) -> Result<Either<Self, TaskLane>, JoinError> {
        self.nr_pending_work += 1;
        self.lane_current.work.push(task);
        self.maybe_await_tasks(config).await
    }

    /// Enqueue a work-task for creation on the `spawn_put`.
    async fn enqueue_work_pending(
        mut self,
        pending: Fut,
        config: &Config,
    ) -> Result<Either<Self, TaskLane>, JoinError> {
        self.nr_pending_work += 1;
        if let Err(_) = self.ser_metadata.send(pending).await {
            panic!("Receiver of metadata queries closed.")
        }
        self.maybe_await_tasks(config).await
    }
}

/// A Future that resolves to a Task.
type FutureTaskBox = Pin<Box<dyn Future<Output = Result<Task, ErrorMetadata>> + Send>>;

#[derive(Debug)]
pub struct Putter {
    assignment: Arc<Assignment>,
    config: Arc<Config>,
    task_awaiter: Option<TaskAwaiter<FutureTaskBox>>,
    /// The satistics of all created tasks.
    ///
    /// This is wrapped in an `RwLock` because it is updated in the put-functions
    /// which are run concurrently.
    result: Arc<RwLock<SyncResult>>,
    /// The `Sender` where complete `TaskLane`s are sent to
    task_sink: Sender<TaskLane>,
    rinfo: Arc<Option<RemoteInfo>>,
    error_channel: UnboundedSender<ErrorFsync>,
}

impl Putter {
    pub fn new(
        assignment: Assignment,
        task_sink: Sender<TaskLane>,
        rinfo: Option<RemoteInfo>,
        config: Config,
        error_channel: UnboundedSender<ErrorFsync>,
    ) -> Self {
        let mut task_awaiter = TaskAwaiter::new(&config, error_channel.clone());
        // The first task is always creation of the root directory. As this directory
        // is never yielded by [`walker::dir_walker`], we have to insert it manually.
        task_awaiter.enqueue_pre_unchecked(Task::CreateDir(assignment.origin_destination.clone()));
        Self {
            assignment: Arc::new(assignment),
            config: Arc::new(config),
            task_awaiter: Some(task_awaiter),
            result: Arc::new(RwLock::new(SyncResult::new())),
            task_sink,
            rinfo: Arc::new(rinfo),
            error_channel,
        }
    }

    /// The replacement of an "async Drop" for the `Putter`.
    ///
    /// This awaits all pendings tasks and sends them to the [`task_sink`](Putter::task_sink) before completing.
    /// It returns the statistics of all tasks created by this `Putter`.
    pub async fn send_lane_and_drop(self) -> Result<SyncResult, ErrorTraverse> {
        let task_awaiter = self.task_awaiter.expect("TaskAwaiter should exist");
        self.task_sink
            .send(task_awaiter.await_tasks_and_drop().await?)
            .await
            .expect("Receiver of tasks closed.");
        let result = *self.result.read().await;
        Ok(result)
    }

    /// Convenience function for the reaction of a call to the enqueue-functions
    /// of [`TaskAwaiter`].
    async fn maybe_send_lane(&mut self, op: Either<TaskAwaiter<FutureTaskBox>, TaskLane>) {
        match op {
            Left(old_manager) => self.task_awaiter = Some(old_manager),
            Right(task_lane) => {
                self.task_sink
                    .send(task_lane)
                    .await
                    .expect("Receiver of tasks closed.");
                self.task_awaiter =
                    Some(TaskAwaiter::new(&self.config, self.error_channel.clone()));
            }
        }
    }

    /// Wrapper around [`_handle_dir_action`](Putter::_handle_dir_action) which is fit for the
    /// [`futures::stream::StreamExt::fold`]-function.
    pub async fn handle_dir_action(
        mut self,
        dir_action: DirAction,
        error_channel: &UnboundedSender<ErrorFsync>,
    ) -> Self {
        match self._handle_dir_action(dir_action).await {
            Ok(()) => self,
            Err(err) => {
                error_channel
                    .send(err.into())
                    .expect("Error channel closed");
                self
            }
        }
    }

    /// Creates a task from `dir_action` by calling the corresponding put-function
    /// (see [`crate::traverse::put`]) and either adds it directly to the
    /// [`task_awaiter`](Putter::task_awaiter) or enqueues the corresponding
    /// future for completion.
    async fn _handle_dir_action(&mut self, dir_action: DirAction) -> Result<(), ErrorTraverse> {
        match dir_action {
            DirAction::Ascend(path) => {
                if let Some(task) = put_mtime(&self.assignment, &path, &self.result).await {
                    // If the enqueue-function exceeds the threshold of tasks in a `TaskLane`
                    // we have to complete the current lane, send it and create a new
                    // `TaskAwaiter`. Otherwise we just reinstate the old `TaskAwaiter`.
                    //
                    // It is done this way because the enqueue-functions of `TaskAwaiter`
                    // are built in such a type-safe function so the number of pending tasks
                    // can never exceed the threshold.
                    let maybe_lane = self
                        .task_awaiter
                        .take()
                        .unwrap()
                        .enqueue_post(task, &self.config)
                        .await?;
                    self.maybe_send_lane(maybe_lane).await;
                }
                Ok(())
            }
            DirAction::Entry(entry, file_type, relative_path) => {
                let name = entry.file_name();
                if file_type.is_dir() {
                    let task = put_directory(&self.assignment, &relative_path, &self.result).await;
                    let maybe_lane = self
                        .task_awaiter
                        .take()
                        .unwrap()
                        .enqueue_pre(task, &self.config)
                        .await?;
                    self.maybe_send_lane(maybe_lane).await;
                    Ok(())
                } else if file_type.is_file() {
                    // Because `put` will be run in a separate spawned task it
                    // needs owned clones of the `Arc`s which contain the `Config`
                    // and so on.
                    let full_relative_path = relative_path.join(&name);
                    let task = put(
                        Arc::clone(&self.assignment),
                        full_relative_path,
                        Arc::clone(&self.config),
                        Arc::clone(&self.result),
                        Arc::clone(&self.rinfo),
                    );
                    let maybe_lane = self
                        .task_awaiter
                        .take()
                        .unwrap()
                        .enqueue_work_pending(Box::pin(task), &self.config)
                        .await?;
                    self.maybe_send_lane(maybe_lane).await;
                    Ok(())
                } else if file_type.is_symlink() {
                    let task = put_symlink(
                        &self.assignment,
                        &relative_path.join(&name),
                        &mut self.result,
                    )
                    .await
                    .map_err(|err| ErrorMetadata::UnkownIfSymlink {
                        path: relative_path.join(&name),
                        err,
                    })?;
                    let maybe_lane = self
                        .task_awaiter
                        .take()
                        .unwrap()
                        .enqueue_work_ready(task, &self.config)
                        .await?;
                    self.maybe_send_lane(maybe_lane).await;
                    Ok(())
                } else {
                    Ok(())
                }
            }
        }
    }
}

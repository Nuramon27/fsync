use serde::{Deserialize, Serialize};

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Copy, Clone, Hash)]
#[derive(Serialize, Deserialize)]
pub struct FileTime {
    pub seconds: i64,
    pub nanos: u32,
}

impl From<filetime::FileTime> for FileTime {
    fn from(value: filetime::FileTime) -> Self {
        FileTime {
            seconds: value.seconds(),
            nanos: value.nanoseconds(),
        }
    }
}

impl From<FileTime> for filetime::FileTime {
    fn from(value: FileTime) -> Self {
        filetime::FileTime::from_unix_time(
            value.seconds - if cfg!(windows) { 11644473600 } else { 0 },
            value.nanos,
        )
    }
}

use std::path::Path;

use tokio::runtime::{self};

use crate::rule::Rule;
use crate::Rules;

use crate::{Assignment, Config};

use super::{sync_print, TEST_DIR, TMP_SUBDIR};

#[test]
fn simple() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("simple");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();
    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("b", "b")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/b", "a_dir/b")),
        "{:?}",
        res
    );
}

#[test]
fn simple_exclude() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("simple");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir")).unwrap()],
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("b", "b")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/b")),
        "{:?}",
        res
    );
}

#[test]
fn simple_exclude_dir() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("simple");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir")).unwrap()],
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("b", "b")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/b")),
        "{:?}",
        res
    );
}

#[test]
fn simple_exclude_file_as_dir() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("simple");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a/")).unwrap()],
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("b", "b")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_destination(&"a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/b", "a_dir/b")),
        "{:?}",
        res
    );
}

#[test]
fn simple_exclude_and_include() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("simple");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("a_dir/a")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("b", "b")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/b")),
        "{:?}",
        res
    );
}

#[test]
fn simple_exclude_and_include_include_relative() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("simple");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("/a_dir/a")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("a_dir/a")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("b", "b")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/b", "a_dir/b")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn simple_exclude_and_include_exclude_relative() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("simple");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir/a")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("/a_dir/a")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("b", "b")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/b", "a_dir/b")),
        "{:?}",
        res
    );
}

#[test]
fn absolute_exclude() {
    let origin_source = TEST_DIR.join("absolute_exclude");
    let origin_destination = TMP_SUBDIR.join("absolute_exclude");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("/a_dir")).unwrap()],
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a", "b_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a", "b_dir/a_dir/a")),
        "{:?}",
        res
    );

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn absolute_exclude_depth_2() {
    let origin_source = TEST_DIR.join("absolute_exclude");
    let origin_destination = TMP_SUBDIR.join("absolute_exclude");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("/a_dir/a_dir")).unwrap()],
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a", "b_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a", "b_dir/a_dir/a")),
        "{:?}",
        res
    );

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn absolute_exclude_incl_vs_excl() {
    let origin_source = TEST_DIR.join("absolute_exclude");
    let origin_destination = TMP_SUBDIR.join("absolute_exclude");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("/b_dir/a_dir")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a", "b_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a", "b_dir/a_dir/a")),
        "{:?}",
        res
    );

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn absolute_exclude_rel_incl_vs_excl() {
    let origin_source = TEST_DIR.join("absolute_exclude");
    let origin_destination = TMP_SUBDIR.join("absolute_exclude");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("b_dir/a_dir")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a", "b_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a", "b_dir/a_dir/a")),
        "{:?}",
        res
    );

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn absolute_glob_exclude() {
    let origin_source = TEST_DIR.join("absolute_exclude");
    let origin_destination = TMP_SUBDIR.join("absolute_exclude");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("/*/a_dir")).unwrap()],
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a", "b_dir/a")),
        "{:?}",
        res
    );

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"b_dir/a_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn deep_rules_relative_different_beginning() {
    let origin_source = TEST_DIR.join("deep_rules");
    let origin_destination = TMP_SUBDIR.join("deep_rules");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir/b_dir")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("b_dir/a_dir")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_dir("b_dir/a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/b_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"b_dir/a_dir/b_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/a_dir/b_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/b_dir/b_dir")),
        "{:?}",
        res
    );

    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a", "b_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a", "b_dir/a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/b_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/a_dir/b_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/b_dir/a_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn deep_rules_deep_include_cancelling_exclude() {
    let origin_source = TEST_DIR.join("deep_rules");
    let origin_destination = TMP_SUBDIR.join("deep_rules");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir/a_dir")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("a_dir/a_dir/a")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir/b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_dir("b_dir/a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/a_dir/b_dir")),
        "{:?}",
        res
    );

    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a_dir/a", "a_dir/a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a_dir/a", "b_dir/a_dir/a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a", "b_dir/a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/a_dir/a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir/b")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"b_dir/a_dir/a_dir/b")),
        "{:?}",
        res
    );
}

#[test]
fn deep_rules_deep_exclude_longer_include() {
    let origin_source = TEST_DIR.join("deep_rules");
    let origin_destination = TMP_SUBDIR.join("deep_rules");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir/a_dir/a")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("a_dir/a_dir")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir/b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_dir("a_dir/a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_dir("b_dir/a_dir/a_dir")),
        "{:?}",
        res
    );

    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a_dir/b", "b_dir/a_dir/a_dir/b")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/a_dir/a_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn deep_rules_include_matches_before_exclude() {
    let origin_source = TEST_DIR.join("deep_rules");
    let origin_destination = TMP_SUBDIR.join("deep_rules");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir/")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("b_dir/a_dir/a")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_dir("b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_dir("b_dir/b_dir/a_dir")),
        "{:?}",
        res
    );

    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"a_dir/b_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"a_dir/b_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"b_dir/a_dir/a_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter()
            .any(|t| t.contains_destination(&"b_dir/a_dir/b_dir")),
        "{:?}",
        res
    );

    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("b_dir/a_dir/a", "b_dir/a_dir/a")),
        "{:?}",
        res
    );
}

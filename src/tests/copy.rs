use std::path::Path;

use tokio::runtime::{self, Runtime};

use crate::rule::Rule;

use crate::sync_fs_local;
use crate::{Assignment, Config, Rules};

use super::{TEST_DIR, TMP_SUBDIR};

#[test]
// ignored because it modifies the disk
#[ignore]
fn simple_exclude() {
    std::fs::create_dir_all(TMP_SUBDIR.clone()).unwrap();
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("remote_simple_dest");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("a_dir")).unwrap()],
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt
        .block_on(sync_fs_local(assignment, config, rules))
        .unwrap();
    assert!(origin_destination.join("a").exists(), "{:?}", res);
    assert!(origin_destination.join("b").exists(), "{:?}", res);
    assert!(!origin_destination.join("a_dir").exists(), "{:?}", res);
    assert!(!origin_destination.join("a_dir/a").exists(), "{:?}", res);
    assert!(!origin_destination.join("a_dir_b").exists(), "{:?}", res);
}

// ignored because it modifies disk
#[test]
#[ignore]
fn simple_link_modified() {
    std::fs::create_dir_all(TMP_SUBDIR.clone()).unwrap();
    let origin_source = TEST_DIR.join("simple_link_modified");
    let mut origin_destination = TMP_SUBDIR.join("copy_simple_link_modified_dest_1");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = Runtime::new().unwrap();
    rt.block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    std::thread::sleep(std::time::Duration::from_millis(10));
    let mut cmd = std::process::Command::new("touch");
    cmd.args([origin_source.join("a")]);
    cmd.output().unwrap();

    let origin_link = std::mem::replace(
        &mut origin_destination,
        TMP_SUBDIR.join("copy_simple_link_modified_dest_2"),
    );
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: Some(origin_link.clone()),
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt
        .block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    assert!(origin_destination.join("a_dir").exists(), "{:?}", res);
    assert!(origin_destination.join("a").exists(), "{:?}", res);
    assert!(origin_destination.join("b").exists(), "{:?}", res);
    assert!(origin_destination.join("a_dir/a").exists(), "{:?}", res);
    assert!(origin_destination.join("a_dir/b").exists(), "{:?}", res);
}

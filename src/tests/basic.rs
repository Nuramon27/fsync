use tokio::runtime;

use crate::Rules;

use crate::{Assignment, Config};

use super::{sync_print, TEST_DIR, TMP_SUBDIR};

#[test]
fn basic() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("simple");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_set_mtime_for_dir("a_dir")),
        "{:?}",
        res
    )
}

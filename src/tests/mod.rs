mod basic;
mod copy;
mod link;
mod many;
mod remote;
mod rules;
mod symlink;

use std::path::{Path, PathBuf};

use lazy_static::lazy_static;

use crate::error::ErrorFsync;
use crate::receive::receive_and_return;

use crate::task::TaskLane;
use crate::traverse::traverse_local;
use crate::{sync_general, Rules};
use crate::{Assignment, Config};

async fn sync_print(
    assignment: Assignment,
    config: Config,
    rules: Rules,
) -> Result<Vec<TaskLane>, Vec<ErrorFsync>> {
    sync_general(
        assignment,
        config,
        rules,
        receive_and_return,
        traverse_local,
    )
    .await
    .map(|(a, _)| a)
}

lazy_static! {
    static ref TEST_DIR: PathBuf = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("test_dir");
    static ref TMP_SUBDIR: PathBuf = std::env::temp_dir().join("fsync_test");
}

#[test]
fn path_retains_trailing_slash() {
    let a = Path::new("a");
    assert_eq!(a.to_str().unwrap(), "a");
    let a_dir = Path::new("a/");
    assert_eq!(a_dir.to_str().unwrap(), "a/");

    let a = PathBuf::from("a");
    assert_eq!(a.to_str().unwrap(), "a");
    let a_dir = PathBuf::from("a/");
    assert_eq!(a_dir.to_str().unwrap(), "a/");
}

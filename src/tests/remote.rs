use tokio::runtime;
use axum_server::Handle;

use crate::remote::RemoteInfo;
use crate::{Assignment, Config};

use super::{TEST_DIR, TMP_SUBDIR};

#[test]
fn remote_basic() {
    let origin_source = TEST_DIR.join("simple");
    let origin_destination = TMP_SUBDIR.join("remote_simple_dest");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = crate::Rules::default();
    let rinfo = RemoteInfo::new("http://127.0.0.1:3000".to_string());

    let rt = runtime::Builder::new_current_thread()
        .enable_io()
        .enable_time()
        .build()
        .unwrap();

    let shutdown_handle = Handle::new();
    let res = rt.block_on(async {
        let serv = tokio::spawn(crate::remote::server::serve_and_print(
            assignment.clone(),
            config.clone(),
            Some(shutdown_handle.clone()),
        ));

        std::thread::sleep(std::time::Duration::from_millis(100));
        let send = tokio::spawn(crate::sync_remote(assignment, config, rules, rinfo));

        let res = send.await.unwrap().unwrap();
        shutdown_handle.graceful_shutdown(None);
        serv.await.unwrap().unwrap();
        res
    });
    println!("{}", res);
}

use tokio::runtime::{self, Runtime};

use crate::sync_fs_local;
use crate::{Assignment, Config, Rules};

use super::{sync_print, TEST_DIR, TMP_SUBDIR};

// ignored because it modifies disk
#[test]
#[ignore]
fn simple() {
    std::fs::create_dir_all(TMP_SUBDIR.clone()).unwrap();
    let origin_source = TEST_DIR.join("simple");
    let mut origin_destination = TMP_SUBDIR.join("simple_link_dest_1");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = Runtime::new().unwrap();
    rt.block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    let origin_link = std::mem::replace(
        &mut origin_destination,
        TMP_SUBDIR.join("simple_link_dest_2"),
    );
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: Some(origin_link.clone()),
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_hardlink("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_hardlink("b", "b")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_hardlink("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_hardlink("a_dir/b", "a_dir/b")),
        "{:?}",
        res
    );
}

// ignored because it modifies disk
#[test]
#[ignore]
fn simple_modified() {
    std::fs::create_dir_all(TMP_SUBDIR.clone()).unwrap();
    let origin_source = TEST_DIR.join("simple_link_modified");
    let mut origin_destination = TMP_SUBDIR.join("simple_modified_dest_1");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = Runtime::new().unwrap();
    rt.block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    std::thread::sleep(std::time::Duration::from_millis(10));
    let mut cmd = std::process::Command::new("touch");
    cmd.args([origin_source.join("a")]);
    cmd.output().unwrap();

    let origin_link = std::mem::replace(
        &mut origin_destination,
        TMP_SUBDIR.join("simple_modified_dest_2"),
    );
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: Some(origin_link.clone()),
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("a", "a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_create_hardlink("a", "a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_hardlink("b", "b")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_hardlink("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_create_hardlink("a_dir/b", "a_dir/b")),
        "{:?}",
        res
    );
}

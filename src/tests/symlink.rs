use std::path::Path;
use std::fs;

use tokio::runtime::{self};

use crate::rule::Rule;

use crate::sync_fs_local;
use crate::{Assignment, Config, Rules};

use super::{sync_print, TEST_DIR, TMP_SUBDIR};

#[test]
// ignored because it modifies the disk
#[ignore]
fn symlink_file() {
    std::fs::create_dir_all(TMP_SUBDIR.clone()).unwrap();
    let origin_source = TEST_DIR.join("symlink_file");
    let origin_destination = TMP_SUBDIR.join("symlink_file_dest");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config::default();
    let rules = Rules::default();

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt
        .block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    assert!(origin_destination.join("a").exists(), "{:?}", res);
    assert!(origin_destination.join("b").exists(), "{:?}", res);
    let metadata = origin_destination.join("b").symlink_metadata().unwrap();
    assert!(
        metadata.is_symlink(),
        "symlink copied as normal file {:?}",
        res
    );
}

/*#[test]
// ignored because it modifies the disk
#[ignore]
fn symlink_file_hardlink_file_modified() {
    let origin_source = TEST_DIR.join("symlink_file");
    let mut origin_destination = TMP_SUBDIR.join("symlink_file_hardlink_file_modified_dest");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None
    };
    let config = Config::default();

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_fs_local(assignment, config, rules)).unwrap();

    std::thread::sleep(std::time::Duration::from_millis(10));
    let mut cmd = std::process::Command::new("touch");
    cmd.args([origin_source.join("a")]);
    cmd.output().unwrap();

    let origin_link = std::mem::replace(&mut origin_destination, TMP_SUBDIR.join("symlink_file_hardlink_file_modified_dest_2"));
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: Some(origin_link.clone()),
    };
    let config = Config {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config)).unwrap();

    assert!(res.iter().any(|t| t.contains_copy_file("a", "a")), "{:?}", res);
    assert!(res.iter().any(|t| t.contains_copy_symlink_file(&"b", &"b", &"a")), "{:?}", res);
}

#[cfg(unix)]
#[test]
// ignored because it modifies the disk
#[ignore]
fn symlink_file_hardlink_link_modified() {
    let origin_source = TEST_DIR.join("symlink_file");
    let mut origin_destination = TMP_SUBDIR.join("symlink_file_hardlink_link_modified_dest");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None
    };
    let config = Config::default();

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let _res = rt.block_on(sync_fs_local(assignment, config, rules)).unwrap();

    std::thread::sleep(std::time::Duration::from_millis(10));
    std::fs::remove_file(origin_source.join("b")).unwrap();
    std::os::unix::fs::symlink(origin_source.join("a"), origin_source.join("b")).unwrap();

    let origin_link = std::mem::replace(&mut origin_destination, TMP_SUBDIR.join("symlink_file_hardlink_link_modified_dest_2"));
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: Some(origin_link.clone()),
    };
    let config = Config {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config)).unwrap();

    assert!(res.iter().any(|t| t.contains_create_hardlink("a", "a")), "{:?}", res);
    assert!(res.iter().any(|t| t.contains_copy_symlink_file(&"b", &"b", &"a")), "{:?}", res);
}*/

#[test]
// ignored because it modifies the disk
#[ignore]
fn symlink_folder() {
    std::fs::create_dir_all(TMP_SUBDIR.clone()).unwrap();
    let origin_source = TEST_DIR.join("symlink_folder");
    let origin_destination = TMP_SUBDIR.join("symlink_folder_dest");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config::default();
    let rules = Rules::default();

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt
        .block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    assert!(origin_destination.join("a_dir").exists(), "{:?}", res);
    assert!(origin_destination.join("b_dir").exists(), "{:?}", res);
    let metadata = origin_destination.join("b_dir").symlink_metadata().unwrap();
    assert!(
        metadata.is_symlink(),
        "symlink copied as normal directory {:?}",
        res
    );
}



#[test]
// ignored because it modifies the disk
#[ignore]
fn symlink_symlink() {
    std::fs::create_dir_all(TMP_SUBDIR.clone()).unwrap();
    let origin_source = TEST_DIR.join("symlink_symlink");
    let origin_destination = TMP_SUBDIR.join("symlink_symlink_dest");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config::default();
    let rules = Rules::default();

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt
        .block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    assert!(origin_destination.join("a").exists(), "{:?}", res);
    assert!(origin_destination.join("b").exists(), "{:?}", res);
    assert!(origin_destination.join("d").exists(), "{:?}", res);
    assert!(!origin_destination.join("e").exists(), "{:?}", res);
    assert!(fs::symlink_metadata(&origin_destination.join("f")).expect("f does not exist").is_symlink(), "{:?}", res);
    assert!(fs::symlink_metadata(&origin_destination.join("g")).expect("g does not exist").is_symlink(), "{:?}", res);
    let metadata = origin_destination.join("b").symlink_metadata().unwrap();
    assert!(
        metadata.is_symlink(),
        "symlink copied as normal file {:?}",
        res
    );
    let metadata = origin_destination.join("d").symlink_metadata().unwrap();
    assert!(
        metadata.is_symlink(),
        "symlink copied as normal file {:?}",
        res
    );
    let metadata = origin_destination.join("f").symlink_metadata().unwrap();
    assert!(
        metadata.is_symlink(),
        "symlink copied as normal file {:?}",
        res
    );
    let metadata = origin_destination.join("g").symlink_metadata().unwrap();
    assert!(
        metadata.is_symlink(),
        "symlink copied as normal file {:?}",
        res
    );
}

#[test]
fn symlink_folder_read_only() {
    let origin_source = TEST_DIR.join("symlink_folder");
    let origin_destination = TMP_SUBDIR.join("symlink_folder");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config::default();
    let rules = Rules::default();

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_symlink_dir(
            "b_dir",
            "b_dir",
            &origin_source.join("a_dir")
        )),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination("b_dir/a")),
        "{:?}",
        res
    );
}

#[test]
fn symlink_folder_exclude() {
    let origin_source = TEST_DIR.join("symlink_folder");
    let origin_destination = TMP_SUBDIR.join("symlink_folder_exclude_dest");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("/b_dir")).unwrap()],
        includes: vec![Rule::from_glob(Path::new("/b_dir/a")).unwrap()],
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_dir("a_dir")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("a_dir/a", "a_dir/a")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"b_dir")),
        "{:?}",
        res
    );
    assert!(
        !res.iter().any(|t| t.contains_destination(&"b_dir/a")),
        "{:?}",
        res
    );
}

#[test]
// ignored because it modifies the disk
#[ignore]
fn symlink_folder_exclude_pointee() {
    std::fs::create_dir_all(TMP_SUBDIR.clone()).unwrap();
    let origin_source = TEST_DIR.join("symlink_folder");
    let origin_destination = TMP_SUBDIR.join("symlink_folder_exclude_pointee");
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        ..Default::default()
    };
    let rules = Rules {
        excludes: vec![Rule::from_glob(Path::new("/a_dir")).unwrap()],
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt
        .block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    assert!(!origin_destination.join("a_dir").exists(), "{:?}", res);
    assert!(!origin_destination.join("a_dir/a").exists(), "{:?}", res);
    assert!(origin_destination.join("b_dir").exists(), "{:?}", res);
    assert!(origin_destination.join("b_dir/a").exists(), "{:?}", res);
    let metadata = origin_destination.join("b_dir").symlink_metadata().unwrap();
    assert!(
        metadata.is_symlink(),
        "symlink copied as normal directory {:?}",
        res
    );
}

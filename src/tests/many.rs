use std::fs;
use std::fs::OpenOptions;
use std::io::Write;
use std::path::Path;

use tokio::runtime::{self, Runtime};

use super::{sync_print, TMP_SUBDIR};
use crate::sync_fs_local;
use crate::{Assignment, Config, Rules};

fn initialize_test_dir(origin_source: &Path) {
    fs::create_dir_all(origin_source).unwrap();
    for i in 0..64 {
        let mut file = OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(&origin_source.join(format!("{:02}", i)))
            .unwrap();
        file.write_all(format!("{:08x}", i * 15).as_bytes())
            .unwrap();
        file.flush().unwrap();
    }
    for i in 0..4 {
        let dir_path = origin_source.join(format!("dir_{:02}", i));
        fs::create_dir(&dir_path).unwrap();
        for j in 0..16 {
            let mut file = OpenOptions::new()
                .write(true)
                .truncate(true)
                .create(true)
                .open(&dir_path.join(format!("{:02}", j)))
                .unwrap();
            file.write_all(format!("dir_{:02x}    content: {:08x}", i, j * 11).as_bytes())
                .unwrap();
            file.flush().unwrap();
        }
    }
}

#[test]
// Ignored because it modifies disk
#[ignore]
fn copy_many_filetime() {
    let origin_source = TMP_SUBDIR.join("many");
    let mut origin_destination = TMP_SUBDIR.join("many_dest_1");
    initialize_test_dir(&origin_source);

    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        max_concurrent_tasks: 8,
        max_tasks_per_lane: 16,
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = Runtime::new().unwrap();
    rt.block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    std::thread::sleep(std::time::Duration::from_millis(10));
    for (i, path) in [
        origin_source.join("04"),
        origin_source.join("08"),
        origin_source.join("dir_01/03"),
    ]
    .into_iter()
    .enumerate()
    {
        let mut file = OpenOptions::new()
            .write(true)
            .truncate(false)
            .create(true)
            .open(&path)
            .unwrap();
        file.write_all(format!("{:04x}", i + 1).as_bytes()).unwrap();
        file.flush().unwrap();
    }
    let mut cmd = std::process::Command::new("touch");
    cmd.args([origin_source.join("16")]);
    cmd.output().unwrap();

    let origin_link = std::mem::replace(&mut origin_destination, TMP_SUBDIR.join("many_dest_2"));
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: Some(origin_link.clone()),
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_hardlink("01", "01")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_hardlink("02", "02")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_hardlink("03", "03")),
        "{:?}",
        res
    );

    assert!(
        res.iter().any(|t| t.contains_copy_file("04", "04")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("08", "08")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("dir_01/03", "dir_01/03")),
        "{:?}",
        res
    );

    assert!(
        res.iter().any(|t| t.contains_copy_file("16", "16")),
        "{:?}",
        res
    );
}

#[test]
// Ignored because it modifies disk
#[ignore]
fn copy_many_checksums() {
    let origin_source = TMP_SUBDIR.join("many_ckecksum");
    let mut origin_destination = TMP_SUBDIR.join("many_ckecksum_dest_1");
    initialize_test_dir(&origin_source);

    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: None,
    };
    let config = Config {
        compare_checksums: true,
        short_circuit_on_different_mtime: true,
        ..Default::default()
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = Runtime::new().unwrap();
    rt.block_on(sync_fs_local(assignment, config, rules))
        .unwrap();

    for (i, path) in [
        origin_destination.join("04"),
        origin_destination.join("08"),
        origin_destination.join("dir_01/03"),
    ]
    .into_iter()
    .enumerate()
    {
        let mut file = OpenOptions::new()
            .write(true)
            .truncate(false)
            .create(true)
            .open(&path)
            .unwrap();
        file.write_all(format!("{:04x}", i + 1).as_bytes()).unwrap();
        file.flush().unwrap();
    }
    let mut cmd = std::process::Command::new("touch");
    cmd.args([origin_source.join("16")]);
    cmd.output().unwrap();

    let origin_link = std::mem::replace(
        &mut origin_destination,
        TMP_SUBDIR.join("many_checksum_dest_2"),
    );
    let assignment = Assignment {
        origin_source: origin_source.clone(),
        origin_destination: origin_destination.clone(),
        origin_link: Some(origin_link.clone()),
    };
    let rules = Rules {
        ..Default::default()
    };

    let rt = runtime::Builder::new_current_thread().build().unwrap();
    let res = rt.block_on(sync_print(assignment, config, rules)).unwrap();

    assert!(
        res.iter().any(|t| t.contains_create_hardlink("01", "01")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_hardlink("02", "02")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_create_hardlink("03", "03")),
        "{:?}",
        res
    );

    assert!(
        res.iter().any(|t| t.contains_copy_file("04", "04")),
        "{:?}",
        res
    );
    assert!(
        res.iter().any(|t| t.contains_copy_file("08", "08")),
        "{:?}",
        res
    );
    assert!(
        res.iter()
            .any(|t| t.contains_copy_file("dir_01/03", "dir_01/03")),
        "{:?}",
        res
    );

    assert!(
        res.iter().any(|t| t.contains_create_hardlink("16", "16")),
        "{:?}",
        res
    );
}

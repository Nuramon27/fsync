//! Contains functions for the _receive-task_ of `fsync`.
//!
//! See [Architecture of `fsync`](crate#architecture-of-fsync)
//! for details.

pub mod receive_local;
pub mod receive_remote;

use std::future::Future;
use std::sync::Arc;

use futures::stream::{self, StreamExt};

use tokio::sync::mpsc::{Receiver, UnboundedSender};

use tokio_stream::wrappers::ReceiverStream;

use crate::error::{ErrorExecute, ErrorFsync};
use crate::task::{Task, TaskLane};
use crate::{Assignment, Config};

use crate::remote::RemoteInfo;
use receive_local::process_task;

/// Use `process_func` to perform `task`.
///
/// If an error occurrs, it is sent down `error_channel`.
pub async fn process_task_and_send_error<
    PFunc: Fn(Arc<Assignment>, Task) -> F,
    F: Future<Output = Result<(), ErrorExecute>>,
>(
    process_func: PFunc,
    assignment: Arc<Assignment>,
    task: Task,
    error_channel: UnboundedSender<ErrorFsync>,
) {
    if let Err(e) = process_func(assignment, task).await {
        error_channel
            .send(e.into())
            .expect("Receiver of errors closed");
    }
}

/// Perform the [`Task`s](crate::task::Task) in `lane`.
///
/// First, the tasks in `lane.pre` are performed sequentially.
/// Then the tasks in `lane.work` are performed concurrently,
/// with at most [`Config::max_concurrent_tasks`](crate::Config::max_concurrent_tasks)
/// tasks being sheduled at the same time. Finally, the tasks in `lane.post`
/// are performed sequentially.
///
/// “Performing” in this case means handing the task to `process_func`.
pub async fn process_task_lane<
    PFunc: Fn(Arc<Assignment>, Task) -> F,
    F: Future<Output = Result<(), ErrorExecute>>,
>(
    process_func: PFunc,
    assignment: Arc<Assignment>,
    config: &Config,
    lane: TaskLane,
    error_channel: &UnboundedSender<ErrorFsync>,
) {
    stream::iter(lane.pre)
        .for_each(|task| async {
            process_task_and_send_error(
                &process_func,
                Arc::clone(&assignment),
                task,
                error_channel.clone(),
            )
            .await;
        })
        .await;
    stream::iter(lane.work)
        .for_each_concurrent(config.max_concurrent_tasks, |task| async {
            process_task_and_send_error(
                &process_func,
                Arc::clone(&assignment),
                task,
                error_channel.clone(),
            )
            .await;
        })
        .await;
    stream::iter(lane.post)
        .for_each(|task| async {
            process_task_and_send_error(
                &process_func,
                Arc::clone(&assignment),
                task,
                error_channel.clone(),
            )
            .await;
        })
        .await;
}

/// Wait for [`Task`s](crate::task::Task) incoming on
/// `messager` and process them.
///
/// If an error occurrs, it is sent down `error_channel`.
pub async fn receive_and_copy_fs(
    assignment: Assignment,
    config: Config,
    messager: Receiver<TaskLane>,
    error_channel: UnboundedSender<ErrorFsync>,
) {
    let mut messager = ReceiverStream::new(messager);
    let assignment = Arc::new(assignment);
    while let Some(lane) = messager.next().await {
        process_task_lane(
            process_task,
            Arc::clone(&assignment),
            &config,
            lane,
            &error_channel,
        )
        .await
    }
}

/// Wait for [`Task`s](crate::task::Task) incoming on
/// `messager` and sent them to a remote site
/// specified by `rinfo`.
///
/// If an error occurrs, it is sent down `error_channel`.
pub async fn receive_and_send_to_remote(
    assignment: Assignment,
    config: Config,
    messager: Receiver<TaskLane>,
    error_channel: UnboundedSender<ErrorFsync>,
    rinfo: RemoteInfo,
) {
    let mut messager = ReceiverStream::new(messager);
    let assignment = Arc::new(assignment);
    while let Some(lane) = messager.next().await {
        process_task_lane(
            |assignment, task| receive_remote::process_task(task, assignment, &rinfo),
            Arc::clone(&assignment),
            &config,
            lane,
            &error_channel,
        )
        .await
    }
}

/// Only collect [`Task`s](crate::task::Task) incoming on `messager`,
/// without performing them.
///
/// This is used for testing
#[cfg(test)]
pub async fn receive_and_return(
    _assignment: Assignment,
    _config: Config,
    messager: Receiver<TaskLane>,
    _error_channel: UnboundedSender<ErrorFsync>,
) -> Vec<TaskLane> {
    let mut res = Vec::new();
    let mut messager = ReceiverStream::new(messager);
    while let Some(lane) = messager.next().await {
        res.push(lane);
    }
    res
}

/// Only print [`Task`s](crate::task::Task) incoming on `messager`,
/// without performing them.
///
/// This can be used for a dry run.
pub async fn receive_and_print(
    _assignment: Assignment,
    _config: Config,
    messager: Receiver<TaskLane>,
    _error_channel: UnboundedSender<ErrorFsync>,
) {
    let mut messager = ReceiverStream::new(messager);
    while let Some(lane) = messager.next().await {
        for task in &lane.pre {
            println!("{}", task);
        }
        for task in &lane.work {
            println!("{}", task);
        }
        for task in &lane.post {
            println!("{}", task);
        }
    }
}

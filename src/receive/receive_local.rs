//! Contains functions for actually performing [`Task`s](crate::task::Task).
//!
//! Despite its name, this module is also necessary in remote operation
//! on the serving site.
//!
//! The functions are mainly self-explanatory. The important one is [`process_task`],
//! which actually performs a single task.

use std::io::{self, ErrorKind};
use std::path::Path;
use std::sync::Arc;

use tokio::fs;

use filetime::set_file_mtime;

use crate::error::ErrorExecute;
use crate::task::Task;
use crate::FileTime;

use crate::Assignment;

#[cfg(unix)]
async fn write_symlink_file(location_link: &Path, target: &Path) -> Result<(), io::Error> {
    fs::symlink(target, location_link).await
}
#[cfg(windows)]
async fn write_symlink_file(location_link: &Path, target: &Path) -> Result<(), io::Error> {
    fs::symlink_file(target, location_link).await
}
#[cfg(not(any(unix, windows)))]
async fn write_symlink_file(location_link: &Path, target: &Path) -> Result<(), io::Error> {
    unimplemented!("Symlinks are not supported on this platform")
}

#[cfg(unix)]
async fn write_symlink_dir(location_link: &Path, target: &Path) -> Result<(), io::Error> {
    fs::symlink(target, location_link).await
}
#[cfg(windows)]
async fn write_symlink_dir(location_link: &Path, target: &Path) -> Result<(), io::Error> {
    fs::symlink_dir(target, location_link).await
}
#[cfg(not(any(unix, windows)))]
async fn write_symlink_dir(location_link: &Path, target: &Path) -> Result<(), io::Error> {
    unimplemented!("Symlinks are not supported on this platform")
}

async fn set_mtime(path: &Path, mtime: FileTime) -> Result<(), io::Error> {
    let path = path.to_owned();
    tokio::task::spawn_blocking(move || set_file_mtime(&path, mtime.into()))
        .await
        .unwrap()
}

async fn copy_file_and_set_mtime(
    src: &Path,
    dest: &Path,
    mtime: Option<FileTime>,
) -> Result<(), io::Error> {
    fs::copy(src, dest).await?;
    if let Some(mtime) = mtime {
        let dest = dest.to_owned();
        tokio::task::spawn_blocking(move || set_file_mtime(&dest, mtime.into()))
            .await
            .unwrap()?;
    }
    Ok(())
}

async fn write_symlink_file_and_set_mtime(
    location_link: &Path,
    target: &Path,
    _mtime: Option<FileTime>,
) -> Result<(), io::Error> {
    write_symlink_file(location_link, target).await?;
    Ok(())
}

async fn write_symlink_dir_and_set_mtime(
    location_link: &Path,
    target: &Path,
    _mtime: Option<FileTime>,
) -> Result<(), io::Error> {
    write_symlink_dir(location_link, target).await?;
    Ok(())
}

pub async fn process_task(assignment: Arc<Assignment>, task: Task) -> Result<(), ErrorExecute> {
    match task.clone() {
        Task::CreateDir(path) => {
            match fs::create_dir(&assignment.origin_destination.join(path)).await {
                Ok(()) => Ok(()),
                Err(err) => match err.kind() {
                    ErrorKind::AlreadyExists => Ok(()),
                    _ => Err(ErrorExecute::OnCreateDir { task, err }),
                },
            }
        }
        Task::SetMTime(path, mtime) => {
            match set_mtime(&assignment.origin_destination.join(path), mtime).await {
                Ok(()) => Ok(()),
                Err(err) => match err.kind() {
                    ErrorKind::AlreadyExists => Ok(()),
                    _ => Err(ErrorExecute::OnSetMTime { task, err }),
                },
            }
        }
        Task::CopyFile {
            src: source,
            dest,
            mtime,
        } => copy_file_and_set_mtime(
            &assignment.origin_source.join(source),
            &assignment.origin_destination.join(dest),
            mtime,
        )
        .await
        .map_err(|err| ErrorExecute::OnCopy { task, err }),
        Task::CreateHardLink {
            src: original,
            dest: link,
        } => fs::hard_link(
            &assignment
                .origin_link
                .as_ref()
                .expect("Task hardlink but no `origin_link` given")
                .join(original),
            &assignment.origin_destination.join(link),
        )
        .await
        .map_err(|err| ErrorExecute::OnLink { task, err }),
        Task::CreateSymlinkFile {
            // src is only in the variant for error-reporting reasons.
            src: _,
            dest,
            content,
            mtime,
        } => write_symlink_file_and_set_mtime(
            &assignment.origin_destination.join(dest),
            &content,
            mtime,
        )
        .await
        .map_err(|err| ErrorExecute::OnSymlinkFile { task, err }),
        Task::CreateSymlinkDir {
            // src is only in the variant for error-reporting reasons.
            src: _,
            dest,
            content,
            mtime,
        } => write_symlink_dir_and_set_mtime(
            &assignment.origin_destination.join(dest),
            &content,
            mtime,
        )
        .await
        .map_err(|err| ErrorExecute::OnSymlinkDir { task, err }),
    }
}

#[cfg(test)]
mod tests {
    use filetime::{set_file_mtime, FileTime};
    #[test]
    // ignored because it modifies the disk
    #[ignore]
    fn set_modification_date() {
        let dir = std::env::temp_dir().join("fsync_test/filetime");
        if !dir.exists() {
            std::fs::create_dir_all(&dir).unwrap();
        }
        let file = dir.join("test_file.txt");
        if !file.exists() {
            std::fs::write(&file, b"Hello").unwrap();
        }
        set_file_mtime(&file, FileTime::from_unix_time(1_262_304_000, 0)).unwrap();
    }
}

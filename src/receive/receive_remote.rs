//! Contains the function [`process_task`] for sending a task
//! to a remote site.

use std::sync::Arc;

use tokio::fs;

use crate::error::ErrorExecute;
use crate::task::{Task, TaskCopyFile};

use crate::remote::RemoteInfo;
use crate::Assignment;

/// Sends `task` to a remote site specified by `rinfo`.
///
/// If the task is a [`Task::CopyFile`],
/// the file is read completely and sent over http to the remote machine.
/// Every other kind of task is sent as is via http, serialized
/// as json.
pub async fn process_task(task: Task, assignment: Arc<Assignment>, rinfo: &RemoteInfo) -> Result<(), ErrorExecute> {
    match task.clone() {
        Task::CopyFile {
            src: source,
            dest,
            mtime,
        } => {
            let content = fs::read(&assignment.origin_source.join(source))
                .await
                .map_err(|err| ErrorExecute::OnCopy {
                    task: task.clone(),
                    err,
                })?;
            let task_remote = TaskCopyFile {
                content,
                dest,
                mtime,
            };
            rinfo
                .client
                .post(format!("{}/copy_file", rinfo.url))
                .json(&task_remote)
                .send()
                .await
                .map(|_| ())
                .map_err(|err| ErrorExecute::Connection { task, err })
        }
        task => rinfo
            .client
            .post(format!("{}/task_other", rinfo.url))
            .json(&task)
            .send()
            .await
            .map(|_| ())
            .map_err(|err| ErrorExecute::Connection { task, err }),
    }
}
